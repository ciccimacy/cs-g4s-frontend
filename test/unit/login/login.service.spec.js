define(['angular', 'angular-mocks', 'login/login.module'], function (notepad) {
  describe('Login Service', function () {
    'use strict';

    var httpBackend;
    var loginEndpointService;
    var q;

    var loggedUser = {
      "email": "admin@admin.com",
      "firstName": "admin",
      "lastName": "admin",
      "admin": true,
      "password": "admin"
    };

    beforeEach(module('loginModule'));

    beforeEach(inject(function ($injector) {

      httpBackend = $injector.get('$httpBackend');
      loginEndpointService = $injector.get('loginEndpointService');
      q = $injector.get('$q');
    }));

    afterEach(function () {
      httpBackend.verifyNoOutstandingExpectation();
      httpBackend.verifyNoOutstandingRequest();
    });

    it('should do an HTTP GET on login()', function () {

      spyOn(loginEndpointService, 'login').and.returnValue(
        q.when(loggedUser));

        loginEndpointService.login().then(function (response) {
          expect(response).toEqual(loggedUser);
        });
      });
    });
  });
