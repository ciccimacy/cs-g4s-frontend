define(['angular', 'angular-mocks', 'administration/administration.module'], function (notepad) {
  describe('Administration Service', function () {
    'use strict';

    var httpBackend;
    var administrationService;
    var administrationEndpointService;
    var q;

    var userAccountList =
    {
      "result": {
        "items" : [
          {
            "email": "admin@admin.com",
            "firstName": "admin",
            "lastName": "admin",
            "admin": true,
            "password": "admin"
          },
          {
            "email": "cicci.miranda@gmail.com",
            "firstName": "Cicci",
            "lastName": "Miranda",
            "admin": true,
            "password": "MACYcute143"
          },
          {
            "email": "useracount@user.com",
            "firstName": "User",
            "lastName": "Account",
            "admin": true,
            "password": "newpassword"
          },
          {
            "email": "test@admin.com",
            "firstName": "testadmin",
            "lastName": "nonadmin",
            "admin": false,
            "password": "nonadmin"
          }

        ]
      }
    };

    var userAccount = {
      email: "useracount@user.com", firstName: "User", lastName: "Account", admin: "true"
    };

    beforeEach(module('administrationModule'));

    beforeEach(inject(function ($injector) {
      administrationService = $injector.get('administrationService');
      httpBackend = $injector.get('$httpBackend');
      administrationEndpointService = $injector.get('administrationEndpointService');
      q = $injector.get('$q');
    }));

    afterEach(function () {
      httpBackend.verifyNoOutstandingExpectation();
      httpBackend.verifyNoOutstandingRequest();
    });

    it('should do an HTTP GET on getAllUserAccount()', function () {

      spyOn(administrationEndpointService, 'getAllUserAccount').and.returnValue(
        q.when(userAccountList));

        administrationService.getAllUserAccount().then(function (response) {
          expect(response).toEqual(userAccountList);
        });
      });

      it('should do an HTTP GET on getLoggedUser()', function () {

        spyOn(administrationEndpointService, 'getLoggedUser').and.returnValue(
          q.when(userAccount));

          administrationService.getLoggedUser("useracount@user.com").then(function (response) {
            expect(response).toEqual(userAccount);
          });
        });

        it('should do an HTTP POST on addUserAccount()', function () {

          spyOn(administrationEndpointService, 'addUserAccount').and.returnValue(
            q.when(11111));

            administrationService.addUserAccount(userAccount).then(function (response) {
              expect(response).toEqual(11111);
            });
          });

          it('should do an HTTP POST on updateLoggedUser()', function () {

            spyOn(administrationEndpointService, 'updateLoggedUser').and.returnValue(
              q.when(123345));

              administrationService.updateLoggedUser(userAccount).then(function (response) {
                expect(response).toEqual(123345);
              });
            });



          });
        });
