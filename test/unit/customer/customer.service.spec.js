define(['angular', 'angular-mocks', 'customer/customer.module'], function (notepad) {
  describe('Customer Service', function () {
    'use strict';

    var httpBackend;
    var customerService;
    var customerEndpointService;
    var q;

    var custList = {
      "result": {
        "items" : [{
          firstName: 'Cicciolina',
          lastName: 'Miranda',
          email: 'cicci.miranda@gmail.com'
        },
        {
          firstName: 'Macy',
          lastName: 'Miranda',
          email: 'macy@miranda@gmail.com'
        },
        {
          firstName: 'Test',
          lastName: 'Miranda',
          email: 'test@gmail.com'
        },
        {
          firstName: 'Cicciolina',
          lastName: 'Miranda',
          email: 'cicci.miranda@gmail.com'
        },
        {
          firstName: 'Macy',
          lastName: 'Miranda',
          email: 'macy@miranda@gmail.com'
        },
        {
          firstName: 'Test',
          lastName: 'Miranda',
          email: 'test@gmail.com'
        },
        {
          firstName: 'Cicciolina',
          lastName: 'Miranda',
          email: 'cicci.miranda@gmail.com'
        },
        {
          firstName: 'Macy',
          lastName: 'Miranda',
          email: 'macy@miranda@gmail.com'
        },
        {
          firstName: 'Test',
          lastName: 'Miranda',
          email: 'test@gmail.com'
        },
        {
          firstName: 'Cicciolina',
          lastName: 'Miranda',
          email: 'cicci.miranda@gmail.com'
        },
        {
          firstName: 'Macy',
          lastName: 'Miranda',
          email: 'macy@miranda@gmail.com'
        },
        {
          firstName: 'Test',
          lastName: 'Miranda',
          email: 'test@gmail.com'
        }]
      }
    };

    var customerContract = {
     "customerDTO": {
      "firstName": "Cicci",
      "lastName": "Awesome",
      "gender": "FEMALE",
      "phoneNumber": "9171212123",
      "birthday": "1867-05-05T00:00:00.000+08:00",
      "companyName": "test company",
      "email": "m@m.com",
      "streetAddress": "test",
      "city": "test city",
      "postcode": 1111,
      "country": "BH",
      "customerNumber": "REF33418280"
     },
     "customerContractDTO": {
      "customerNumber": "REF33418280",
      "contractNum": [
       "CONTRACT80841550",
       "CONTRACT13225450",
       "CONTRACT28454981",
       "CONTRACT82290345"
      ]
     }
   };

   var newCustomer = {};

    beforeEach(module('customerModule'));

    beforeEach(inject(function ($injector) {
      customerService = $injector.get('customerService');
      httpBackend = $injector.get('$httpBackend');
      customerEndpointService = $injector.get('customerEndpointService');
      q = $injector.get('$q');
    }));

    afterEach(function () {
      httpBackend.verifyNoOutstandingExpectation();
      httpBackend.verifyNoOutstandingRequest();
    });

    it('should do an HTTP GET on getCustomers()', function () {

      spyOn(customerEndpointService, 'getCustomers').and.returnValue(
        q.when(custList));

      customerService.getCustomers().then(function (response) {
        expect(response).toEqual(custList);
      });
    });

    it('should do an HTTP GET on getExistingCustomerContracts()', function () {

      spyOn(customerEndpointService, 'getExistingCustomerContracts').and.returnValue(
        q.when(customerContract));

      customerService.getExistingCustomerContracts("REF33418280").then(function (response) {
        expect(response).toEqual(customerContract);
      });
    });

    it('should do an HTTP POST on addCustomerContact()', function () {

      spyOn(customerEndpointService, 'addCustomerContact').and.returnValue(
        q.when(1234567));

      customerService.addCustomerContact(customerContract).then(function (response) {
        expect(response).toEqual(1234567);
      });
    });

    it('should do an HTTP POST on updateCustomerContract()', function () {

      spyOn(customerEndpointService, 'updateCustomerContract').and.returnValue(
        q.when(77777));

      customerService.updateCustomerContract(customerContract).then(function (response) {
        expect(response).toEqual(77777);
      });
    });



  });
});
