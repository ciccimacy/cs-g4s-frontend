define(['angular', 'angular-mocks', 'contract/contract.module'], function (notepad) {
  describe('Contract Service', function () {
    'use strict';

    var httpBackend;
    var contractService;
    var contractEndpointService;
    var q;

    var contractList =
    {
      "result": {
        "items" : [
          {
            contractNum: "CONTRACT27296245",
            title: "Order 101",
            body: "At vero eos et accusamus et iusto odio dignissimos"
          },
          {
            contractNum: "CONTRACT27296246",
            title: "Order 102",
            body: "At vero eos et accusamus et iusto odio dignissimosur"
          },
          {
            contractNum: "CONTRACT27296244",
            title: "Order 103",
            body: "At vero eos et accusamus et iusto odio dignissimos"
          }
        ]
      }
    };

    var contract = {
      "contractNum": "CONTRACT80841550",
      "title": "Order 101",
      "body": "At vero eos et accusamus et iusto odio dignissimos"
    };

    beforeEach(module('contractModule'));

    beforeEach(inject(function ($injector) {
      contractService = $injector.get('contractService');
      httpBackend = $injector.get('$httpBackend');
      contractEndpointService = $injector.get('contractEndpointService');
      q = $injector.get('$q');
    }));

    afterEach(function () {
      httpBackend.verifyNoOutstandingExpectation();
      httpBackend.verifyNoOutstandingRequest();
    });

    it('should do an HTTP GET on getContracts()', function () {

      spyOn(contractEndpointService, 'getContracts').and.returnValue(
        q.when(contractList));

      contractService.getContracts().then(function (response) {
        expect(response).toEqual(contractList);
      });
    });

    it('should do an HTTP GET on getExistingContract()', function () {

      spyOn(contractEndpointService, 'getExistingContract').and.returnValue(
        q.when(contract));

      contractService.getExistingContract("CONTRACT80841550").then(function (response) {
        expect(response).toEqual(contract);
      });
    });

    it('should do an HTTP POST on addContract()', function () {

      spyOn(contractEndpointService, 'addContract').and.returnValue(
        q.when(1234567));

      contractService.addContract(contract).then(function (response) {
        expect(response).toEqual(1234567);
      });
    });

    it('should do an HTTP POST on updateContract()', function () {

      spyOn(contractEndpointService, 'updateContract').and.returnValue(
        q.when(77777));

      contractService.updateContract(contract).then(function (response) {
        expect(response).toEqual(77777);
      });
    });



  });
});
