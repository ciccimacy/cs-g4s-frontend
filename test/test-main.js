'use strict';

var allTestFiles = [];
var TEST_REGEXP = /(spec|test)\.js$/i;

// Get a list of all the test files to include
Object.keys(window.__karma__.files).forEach(function (file) {
  if (TEST_REGEXP.test(file)) {
    // Normalize paths to RequireJS module names.
    // If you require sub-dependencies of test files to be loaded as-is (requiring file extension)
    // then do not normalize the paths
    //var normalizedTestModule = file.replace(/^\/base\/|\.js$/g, '');
    allTestFiles.push(file);
  }
});

require.config({
  paths: {
    angular: '../../bower_components/angular/angular',
    'angular-route': '../../bower_components/angular-route/angular-route',
    bootstrap: '../../bower_components/bootstrap/dist/js/bootstrap',
    jquery: '../../bower_components/jquery/dist/jquery',
    'angular-mocks': '../../bower_components/angular-mocks/angular-mocks',
    moment: '../../bower_components/moment/moment',
    interact: '../../bower_components/interact/interact',
    'angular-bootstrap': '../../bower_components/angular-bootstrap/ui-bootstrap-tpls',
    'angular-cookies': '../../bower_components/angular-cookies/angular-cookies',
    'dir-paginate':'../../bower_components/angular-utils-pagination/dirPagination',
    'angularModalService':'../../bower_components/angular-modal-service/dst/angular-modal-service',
    'lodash':'../../bower_components/lodash/lodash.min',
    'jspdf':'../../bower_components/jspdf/dist/jspdf.min',
    'customer-endpoint-service': 'customer/customer.mock.endpoint.service',
    'administration-endpoint-service': 'administration/administration.mock.endpoint.service',
    'contract-endpoint-service': 'contract/contract.mock.endpoint.service',
    'loginEndpointService': 'login/login.mock.endpoint.service'
  },
  shim: {
    bootstrap: {
      deps: [
        'jquery'
      ]
    },
    angular: {
      exports: 'angular'
    },
    'angular-route': [
      'angular'
    ],
    // 'angular-ui-select': [
    //   'angular'
    // ],
    'angular-mocks': [
      'angular'
    ],
    // 'angular-sanitize': [
    //   'angular'
    // ],
    'angular-bootstrap': [
      'angular'
    ],
    'angular-cookies': [
      'angular'
    ],
    'dir-paginate': {
      deps: [
        'angular'
      ]
    },
    'angularModalService': [
      'angular'
    ]
    // cryptojs: {
    //   exports: 'CryptoJS'
    // }
  },

  baseUrl: '/base/app/modules',

  // dynamically load all test files
  deps: allTestFiles,

  // we have to kickoff jasmine, as it is asynchronous
  callback: window.__karma__.start
});
