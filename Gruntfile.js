module.exports = function (grunt) {
  var rewrite = require('connect-modrewrite');
  var globalConfig = {
    images: 'app/images',
    css: 'app/styles',
    fonts: 'app/fonts',
    scripts: 'app/modules',
    bower_path: 'bower_components',
    distribution: 'src/main/webapp'
  };

  grunt.initConfig({
    globalConfig: globalConfig,
    clean: ['<%= globalConfig.distribution %>/fonts','<%= globalConfig.distribution %>/images','<%= globalConfig.distribution %>/modules','<%= globalConfig.distribution %>/styles','<%= globalConfig.distribution %>/*.html','<%= globalConfig.distribution %>/*.js', '.tmp', '<%= globalConfig.fonts %>/*.*', 'app/require.js', '<%= globalConfig.css %>/compiled-bootstrap', '<%= globalConfig.css %>/*.*'],
    requirejs: {
      main: {
        options: {
          baseUrl: '<%= globalConfig.scripts %>',
          mainConfigFile: '<%= globalConfig.scripts %>/main.js',
          name: 'main',
          out: '<%= globalConfig.distribution %>/modules/main.js',
          optimize: "none",
          preserveLicenseComments: false
        }
      },
      signin: {
        options: {
          baseUrl: '<%= globalConfig.scripts %>',
          mainConfigFile: '<%= globalConfig.scripts %>/signin-main.js',
          name: 'signin-main',
          out: '<%= globalConfig.distribution %>/modules/signin-main.js',
          optimize: "none",
          preserveLicenseComments: false
        }
      }
    },
    uglify: {
      options: {
        mangle: true,
        compress: true,
        files: {
          '<%= globalConfig.distribution %>/modules/main.js': ['<%= requirejs.main.out %>'],
          '<%= globalConfig.distribution %>/modules/signin-main.js': ['<%= requirejs.signin.out %>']
        }
      }
    },
    connect: {
      options: {
        port: 9000,
        middleware: function (connect) {
          return [
            connect().use(
              '/bower_components',
              connect.static('./bower_components')
            ),
            connect().use(
              '/node_modules',
              connect.static('./node_modules')
            ),
            connect().use(
              '/login',
              './app/index.html'
            ),
            connect.static('./app')
          ];
        }
      },
      server: {}
    },
    karma: {
        unit: {
            configFile: 'karma.conf.js',
            singleRun: true
        }
    },
    bowerRequirejs: {
      target: {
        rjsConfig: '<%= globalConfig.scripts %>/main.js',
        options: {
          transitive: true
        }
      }
    },
    copy: {
      pages: {
        files: [
          {
            expand: true,
            cwd: 'app',
            src: ['**/*.html'],
            dest: '<%= globalConfig.distribution %>/',
            filter: 'isFile'
          }
        ]
      },
      statics: {
        files: [
          {
            expand: true,
            cwd: 'app',
            src: ['fonts/**', 'images/**','styles/**'],
            dest: '<%= globalConfig.distribution %>/',
            filter: 'isFile'
          }
        ]
      },
      require: {
        files: [
          {
            expand: true,
            cwd: 'app',
            src: ['require.js'],
            dest: '<%= globalConfig.distribution %>/',
            filter: 'isFile'
          }
        ]
      },
      init: {
        files: [
          {
            expand: true,
            flatten: true,
            src: ['node_modules/requirejs/require.js'],
            dest: 'app/',
            filter: 'isFile'
          },
          //{
          //  expand: true,
          //  flatten: true,
          //  src: ['bower_components/bootstrap/dist/css/bootstrap-theme.min.css'],
          //  dest: '<%= globalConfig.css %>/raw',
          //  filter: 'isFile'
          //},
          {
            expand: true,
            flatten: true,
            src: ['<%= globalConfig.bower_path %>/bootstrap/dist/fonts/*'],
            dest: '<%= globalConfig.fonts %>/',
            filter: 'isFile'
          },
          {
            expand: true,
            flatten: true,
            src: ['<%= globalConfig.bower_path %>/font-awesome/fonts/*'],
            dest: '<%= globalConfig.fonts %>/',
            filter: 'isFile'
          }
        ]
      }
    },
    useminPrepare: {
      html: ['app/**/*.html'],
      options: {
        dest: '<%= globalConfig.distribution %>'
      }
    },
    usemin: {
      html: ['<%= globalConfig.distribution %>/**/*.html'],
      options: {
        assetsDirs: ['<%= globalConfig.distribution %>']
      }
    },
    filerev: {
      dist: {
        src: [
          '<%= globalConfig.distribution %>/styles/**/*.css',
        ]
      }
    },
    watch: {
      scripts: {
        files: ['app/**/*.{js,html,css}']
      }
    }
  });


  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-filerev');
  grunt.loadNpmTasks('grunt-bower-requirejs');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-usemin');

  grunt.registerTask('init', ['clean', 'copy:init']);
  grunt.registerTask('serve', ['connect', 'watch']);
  grunt.registerTask('test', ['init', 'karma']);
  grunt.registerTask('release', ['test', 'requirejs', 'copy:pages', 'copy:require', 'copy:statics','build-pages']);
  grunt.registerTask('build-pages', ['useminPrepare', 'concat:generated', 'cssmin:generated', 'filerev', 'usemin']);
};
