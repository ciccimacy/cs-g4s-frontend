package com.cloudsherpas.secured.api;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cloudsherpas.secured.dto.UserAccountDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;
import com.cloudsherpas.secured.service.UserAccountService;

@RunWith(MockitoJUnitRunner.class)
public class UserAccountServiceResourceTest {

    @InjectMocks
    UserAccountResource userAccountResource = new UserAccountResource();

    @Mock
    UserAccountService userAccountService;

    UserAccountDTO userAccountDTO;


    @Before
    public void setUp() {
        setUpUserAccountDTO();
    }

    private void setUpUserAccountDTO() {
        userAccountDTO = new UserAccountDTO();
        userAccountDTO.setAdmin(true);
        userAccountDTO.setEmail("test@gmail.com");
        userAccountDTO.setFirstName("Juan");
        userAccountDTO.setLastName("Ramos");
        userAccountDTO.setPassword("newpassword");
    }

    @Test
    public void testGetAllUserAccount() throws SecuredException {
        List<UserAccountDTO> expectedList = new ArrayList<UserAccountDTO>();
        expectedList.add(userAccountDTO);
        when(userAccountService.getAllUserAccount())
                .thenReturn(expectedList);
        List<UserAccountDTO> results = userAccountResource.getAllUserAccount();
        assertEquals(1,results.size());
    }

    @Test
    public void testAddUserAccount() throws SecuredException{
        when(userAccountService.addUserAccount(userAccountDTO)).thenReturn(1234l);
        Map<String, Long> result = userAccountResource.addUserAccount(userAccountDTO);
        Assert.assertNotNull(result);
    }

    @Test
    public void testGetUserAccountById() throws SecuredException{
        when(userAccountService.getUserAccountByEmail("test@gmail.com")).thenReturn(userAccountDTO);
        UserAccountDTO result = userAccountResource.getUserAccountByEmail("test@gmail.com");
        Assert.assertNotNull(result);
    }

	@Test
	public void testVerifyLogin() throws SecuredException{
	    when(userAccountService.verifyLogin("test@gmail.com","newpassword")).thenReturn(userAccountDTO);
	    UserAccountDTO result = userAccountResource.verifyLogin("test@gmail.com","newpassword");
	    Assert.assertNotNull(result);
	}
}
