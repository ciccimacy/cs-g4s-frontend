package com.cloudsherpas.secured.api;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cloudsherpas.secured.dto.ContractDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;
import com.cloudsherpas.secured.service.ContractService;

@RunWith(MockitoJUnitRunner.class)
public class ContractResourceTest {

    @InjectMocks
    ContractResource contractResource = new ContractResource();

    @Mock
    ContractService contractService;

    ContractDTO contractDTO;

    @Before
    public void setUp() {
        setUpContractDTO();
    }

    private void setUpContractDTO() {
        contractDTO = new ContractDTO();
        contractDTO.setContractNum("CONTRACT001");
        contractDTO.setTitle("Title");
        contractDTO.setBody("Body");
    }

    @Test
    public void testGetAllContract() throws SecuredException {
        List<ContractDTO> expectedList = new ArrayList<ContractDTO>();
        expectedList.add(contractDTO);
        when(contractService.getAllContracts())
                .thenReturn(expectedList);
        List<ContractDTO> results = contractResource.getAllContracts();
        assertEquals(1,results.size());
    }

    @Test
    public void testAddContract() throws SecuredException{
        when(contractService.addContract(contractDTO)).thenReturn(1234l);
        Map<String, Long> result = contractResource.addContract(contractDTO);
        Assert.assertNotNull(result);
    }

    @Test
    public void testGetContractById() throws SecuredException{
        when(contractService.getContractByContractNum("CONTRACT001")).thenReturn(contractDTO);
        ContractDTO result = contractResource.getContractByContractNum("CONTRACT001");
        Assert.assertNotNull(result);
    }
}
