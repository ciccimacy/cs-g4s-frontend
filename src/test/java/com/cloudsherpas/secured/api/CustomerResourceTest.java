package com.cloudsherpas.secured.api;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cloudsherpas.secured.dto.CustomerDTO;
import com.cloudsherpas.secured.enums.GenderEnum;
import com.cloudsherpas.secured.exceptions.SecuredException;
import com.cloudsherpas.secured.service.CustomerService;

@RunWith(MockitoJUnitRunner.class)
public class CustomerResourceTest {

    @InjectMocks
    CustomerResource customerResource = new CustomerResource();

    @Mock
    CustomerService customerService;

    CustomerDTO customerDTO;


    @Before
    public void setUp() {
        setUpCustomerDTO();
    }

    private void setUpCustomerDTO() {
        customerDTO = new CustomerDTO();
        customerDTO.setCity("Manila");
        customerDTO.setCompanyName("Cloud Sherpas");
        customerDTO.setCountry("Philippines");
        customerDTO.setEmail("test@gmail.com");
        customerDTO.setPhoneNumber("09171234567");
        customerDTO.setFirstName("Juan");
        customerDTO.setLastName("Dela Cruz");
        customerDTO.setGender(GenderEnum.FEMALE);
        customerDTO.setBirthdayStr("05-05-1991");
        customerDTO.setCustomerNumber("REF12341234");
    }


    @Test
    public void testGetAllCustomer() throws SecuredException {
        List<CustomerDTO> expectedList = new ArrayList<CustomerDTO>();
        expectedList.add(customerDTO);
        when(customerService.getAllCustomer())
                .thenReturn(expectedList);
        List<CustomerDTO> results = customerResource.getAllCustomer();
        assertEquals(1,results.size());
    }

    @Test
    public void testAddCustomer() throws SecuredException{
        when(customerService.addCustomer(customerDTO)).thenReturn(1234l);
        Long result = customerService.addCustomer(customerDTO);
        Assert.assertNotNull(result);
    }

    @Test
    public void testUpdateCustomer() throws SecuredException{
        CustomerDTO customerDTO2 = new CustomerDTO();
        customerDTO2.setCity("Manila");
        customerDTO2.setCompanyName("Cloud Sherpas");
        customerDTO2.setCountry("Philippines");
        customerDTO2.setEmail("test@gmail.com");
        customerDTO2.setPhoneNumber("09171234568");
        customerDTO2.setFirstName("Juan");
        customerDTO2.setLastName("Dela Cruz");
        customerDTO2.setGender(GenderEnum.FEMALE);
        customerDTO2.setBirthdayStr("05-05-1991");
        customerDTO2.setCustomerNumber("REF12341234");
        when(customerService.updateCustomer(customerDTO2)).thenReturn(1234l);
        Long result = customerService.addCustomer(customerDTO2);
        Assert.assertNotNull(result);
    }

    @Test
    public void testGetCustomerById() throws SecuredException{
        when(customerService.getCustomerByID("REF12341234")).thenReturn(customerDTO);
        CustomerDTO result = customerService.getCustomerByID("REF12341234");
        Assert.assertNotNull(result);
    }
}
