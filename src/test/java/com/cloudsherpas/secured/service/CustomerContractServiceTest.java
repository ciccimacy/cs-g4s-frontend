package com.cloudsherpas.secured.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cloudsherpas.secured.constants.CustomerContractDAO;
import com.cloudsherpas.secured.dto.CustomerContractDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;
import com.cloudsherpas.secured.models.CustomerContractModel;
import com.cloudsherpas.secured.service.impl.CustomerContractServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CustomerContractServiceTest {

    @InjectMocks
    CustomerContractService customerContractService = new CustomerContractServiceImpl();

    @Mock
    CustomerContractDAO customerContractDAO;

    CustomerContractDTO customerContractDTO;

    CustomerContractModel customerContractModel;

    @Before
    public void setUp() {
        setUpCustomerContractDTO();
        setUpCustomerContractModel();
    }

    private void setUpCustomerContractDTO() {
        customerContractDTO = new CustomerContractDTO();
    }

    private void setUpCustomerContractModel() {
        customerContractModel = new CustomerContractModel();
    }

    @Test
    public void testAddCustomer() throws SecuredException {
        when(customerContractDAO.put(customerContractModel)).thenReturn(1234l);
        Long result = customerContractService
                .addCustomerContracts(customerContractDTO);
        Assert.assertNotNull(result);
    }

    @Test
    public void testGetCustomerContractByCustomerNumber()
            throws SecuredException {
        List<CustomerContractModel> expectedList = new ArrayList<CustomerContractModel>();
        expectedList.add(customerContractModel);
        when(
                customerContractDAO
                        .getCustomerAndContractByCustomerNumber("REF12341234"))
                .thenReturn(expectedList);
        CustomerContractDTO result = customerContractService
                .getContractsForCustomer("REF12341234");
        Assert.assertNotNull(result);
    }
}
