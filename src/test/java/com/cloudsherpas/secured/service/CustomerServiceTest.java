package com.cloudsherpas.secured.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cloudsherpas.secured.dao.CustomerDAO;
import com.cloudsherpas.secured.dto.CustomerDTO;
import com.cloudsherpas.secured.enums.GenderEnum;
import com.cloudsherpas.secured.exceptions.SecuredException;
import com.cloudsherpas.secured.models.CustomerModel;
import com.cloudsherpas.secured.service.impl.CustomerServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

    @InjectMocks
    CustomerService customerService = new CustomerServiceImpl();

    @Mock
    CustomerDAO customerDAO;

    CustomerDTO customerDTO;

    CustomerModel customerModel;

    @Before
    public void setUp() {
        setUpCustomerDTO();
        setUpCustomerModel();
    }

    private void setUpCustomerDTO() {
        customerDTO = new CustomerDTO();
        customerDTO.setCity("Manila");
        customerDTO.setCompanyName("Cloud Sherpas");
        customerDTO.setCountry("Philippines");
        customerDTO.setEmail("test@gmail.com");
        customerDTO.setPhoneNumber("09171234567");
        customerDTO.setFirstName("Juan");
        customerDTO.setLastName("Dela Cruz");
        customerDTO.setGender(GenderEnum.FEMALE);
        customerDTO.setBirthdayStr("05-05-1991");
        customerDTO.setCustomerNumber("REF12341234");
    }

    private void setUpCustomerModel() {
        customerModel = new CustomerModel();
        customerModel.setId(1234l);
        customerModel.setCity("Manila");
        customerModel.setCompanyName("Cloud Sherpas");
        customerModel.setCountry("Philippines");
        customerModel.setEmail("test@gmail.com");
        customerModel.setPhoneNumber("09171234567");
        customerModel.setFirstName("Juan");
        customerModel.setLastName("Dela Cruz");
        customerModel.setGender(GenderEnum.FEMALE);
        customerModel.setCustomerNumber("REF12341234");
    }

    @Test
    public void testGetAllCustomer() throws SecuredException {
        List<CustomerModel> expectedList = new ArrayList<CustomerModel>();
        expectedList.add(customerModel);
        when(customerDAO.getAll())
                .thenReturn(expectedList);
        List<CustomerDTO> results = customerService.getAllCustomer();
        assertEquals(1,results.size());
    }

    @Test
    public void testAddCustomer() throws SecuredException{
        when(customerDAO.put(customerModel)).thenReturn(1234l);
        Long result = customerService.addCustomer(customerDTO);
        Assert.assertNotNull(result);
    }

    @Test
    public void testGetCustomerById() throws SecuredException{
        List<CustomerModel> expectedList = new ArrayList<CustomerModel>();
        expectedList.add(customerModel);
        when(customerDAO.getRecordByCustomerNumber("REF12341234")).thenReturn(expectedList);
        CustomerDTO result = customerService.getCustomerByID("REF12341234");
        Assert.assertNotNull(result);
    }

}
