package com.cloudsherpas.secured.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cloudsherpas.secured.dao.ContractDAO;
import com.cloudsherpas.secured.dto.ContractDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;
import com.cloudsherpas.secured.models.ContractModel;
import com.cloudsherpas.secured.service.impl.ContractServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ContractServiceTest {

    @InjectMocks
    ContractService contractService = new ContractServiceImpl();

    @Mock
    ContractDAO contractDAO;

    ContractDTO contractDTO;

    ContractModel contractModel;

    @Before
    public void setUp() {
        setUpContractDTO();
        setUpContractModel();
    }

    private void setUpContractDTO() {
        contractDTO = new ContractDTO();
        contractDTO.setContractNum("CONTRACT001");
        contractDTO.setTitle("Title");
        contractDTO.setBody("Body");
    }

    private void setUpContractModel() {
        contractModel = new ContractModel();
        contractModel.setContractNum("CONTRACT001");
        contractModel.setTitle("Title");
        contractModel.setBody("Body");
        contractModel.setId(1234l);
    }

    @Test
    public void testGetAllContract() throws SecuredException {
        List<ContractModel> expectedList = new ArrayList<ContractModel>();
        expectedList.add(contractModel);
        when(contractDAO.getAll())
                .thenReturn(expectedList);
        List<ContractDTO> results = contractService.getAllContracts();
        assertEquals(1,results.size());
    }

    @Test
    public void testAddContract() throws SecuredException{
        when(contractDAO.put(contractModel)).thenReturn(1234l);
        Long result = contractService.addContract(contractDTO);
        Assert.assertNotNull(result);
    }

    @Test
    public void testGetContractById() throws SecuredException{
        List<ContractModel> expectedList = new ArrayList<ContractModel>();
        expectedList.add(contractModel);
        when(contractDAO.getContractByContractNum("CONTRACT001")).thenReturn(expectedList);
        ContractDTO result = contractService.getContractByContractNum("CONTRACT001");
        Assert.assertNotNull(result);
    }
}
