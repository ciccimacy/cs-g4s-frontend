package com.cloudsherpas.secured.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cloudsherpas.secured.dao.UserAccountDAO;
import com.cloudsherpas.secured.dto.UserAccountDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;
import com.cloudsherpas.secured.models.UserAccountModel;
import com.cloudsherpas.secured.service.impl.UserAccountServiceImpl;
import com.google.appengine.repackaged.com.google.api.client.util.Base64;

@RunWith(MockitoJUnitRunner.class)
public class UserAccountServiceTest {
    @InjectMocks
    UserAccountService userAccountService = new UserAccountServiceImpl();

    @Mock
    UserAccountDAO userAccountDAO;

    UserAccountDTO userAccountDTO;

    UserAccountModel userAccountModel;

    @Before
    public void setUp() {
        setUpUserAccountDTO();
        setUpUserAccountModel();
    }

    private void setUpUserAccountDTO() {
        userAccountDTO = new UserAccountDTO();
        userAccountDTO.setAdmin(true);
        userAccountDTO.setEmail("test@gmail.com");
        userAccountDTO.setFirstName("Juan");
        userAccountDTO.setLastName("Ramos");
        userAccountDTO.setPassword("newpassword");
    }

    private void setUpUserAccountModel() {
        userAccountModel = new UserAccountModel();
        userAccountModel.setAdmin(true);
        userAccountModel.setEmail("test@gmail.com");
        userAccountModel.setFirstName("Juan");
        userAccountModel.setLastName("Ramos");
        userAccountModel.setPassword("newpassword");
    }

    @Test
    public void testGetAllUserAccount() throws SecuredException {
        List<UserAccountModel> expectedList = new ArrayList<UserAccountModel>();
        expectedList.add(userAccountModel);
        when(userAccountDAO.getAll())
                .thenReturn(expectedList);
        List<UserAccountDTO> results = userAccountService.getAllUserAccount();
        assertEquals(1,results.size());
    }

    @Test
    public void testAddUserAccount() throws SecuredException{
        when(userAccountDAO.put(userAccountModel)).thenReturn(1234l);
        Long result = userAccountService.addUserAccount(userAccountDTO);
        Assert.assertNotNull(result);
    }

    @Test
    public void testGetUserAccountById() throws SecuredException{
        List<UserAccountModel> expectedList = new ArrayList<UserAccountModel>();
        expectedList.add(userAccountModel);
        when(userAccountDAO.getUserAccountByEmail("test@gmail.com")).thenReturn(expectedList);
        UserAccountDTO result = userAccountService.getUserAccountByEmail("test@gmail.com");
        Assert.assertNotNull(result);
    }

    @Test
    public void testVerifyLogin() throws SecuredException{
        List<UserAccountModel> expectedList = new ArrayList<UserAccountModel>();
        expectedList.add(userAccountModel);
        when(userAccountDAO.verifyLogin("test@gmail.com",encryptPassword("newpassword"))).thenReturn(expectedList);
        UserAccountDTO result = userAccountService.verifyLogin("test@gmail.com","newpassword");
        Assert.assertNotNull(result);
    }

    private String encryptPassword(final String password)
            throws SecuredException {
        String encryptedText = "";
        if (password != null) {
            encryptedText = new String(Base64.encodeBase64(password.getBytes()));
        }
        return encryptedText;
    }
}