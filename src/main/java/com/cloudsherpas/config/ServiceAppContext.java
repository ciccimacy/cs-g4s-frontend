package com.cloudsherpas.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.cloudsherpas.secured.constants.ServiceConstants;
import com.cloudsherpas.secured.service.ContractService;
import com.cloudsherpas.secured.service.CustomerContractService;
import com.cloudsherpas.secured.service.CustomerService;
import com.cloudsherpas.secured.service.UserAccountService;
import com.cloudsherpas.secured.service.impl.ContractServiceImpl;
import com.cloudsherpas.secured.service.impl.CustomerContractServiceImpl;
import com.cloudsherpas.secured.service.impl.CustomerServiceImpl;
import com.cloudsherpas.secured.service.impl.UserAccountServiceImpl;

@Configuration
@Lazy
public class ServiceAppContext {

    @Bean(name = ServiceConstants.CUSTOMER_SERVICE)
    public CustomerService getCustomerService() {
        return new CustomerServiceImpl();
    }

    @Bean(name = ServiceConstants.USER_SERVICE)
    public UserAccountService getUserService() {
        return new UserAccountServiceImpl();
    }

    @Bean(name = ServiceConstants.CONTRACT_SERVICE)
    public ContractService getContractService() {
        return new ContractServiceImpl();
    }

    @Bean(name = ServiceConstants.CUSTOMER_CONTRACT_SERVICE)
    public CustomerContractService getCustomerContractService() {
        return new CustomerContractServiceImpl();
    }

}
