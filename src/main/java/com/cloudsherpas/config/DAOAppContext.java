package com.cloudsherpas.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import com.cloudsherpas.secured.constants.CustomerContractDAO;
import com.cloudsherpas.secured.constants.DAOConstants;
import com.cloudsherpas.secured.dao.ContractDAO;
import com.cloudsherpas.secured.dao.CustomerDAO;
import com.cloudsherpas.secured.dao.UserAccountDAO;
import com.cloudsherpas.secured.dao.impl.ContractDAOImpl;
import com.cloudsherpas.secured.dao.impl.CustomerContractDAOImpl;
import com.cloudsherpas.secured.dao.impl.CustomerDAOImpl;
import com.cloudsherpas.secured.dao.impl.UserAccountDAOImpl;

@Configuration
@Lazy
public class DAOAppContext {

    @Bean(name = DAOConstants.CUSTOMER_DAO)
    public CustomerDAO getCustomerDAO() {
        return new CustomerDAOImpl();
    }

    @Bean(name = DAOConstants.USER_DAO)
    public UserAccountDAO getUserDAO() {
        return new UserAccountDAOImpl();
    }

    @Bean(name = DAOConstants.CONTRACT_DAO)
    public ContractDAO getContractDAO() {
        return new ContractDAOImpl();
    }

    @Bean(name = DAOConstants.CUSTOMER_CONTRACT_DAO)
    public CustomerContractDAO getCustomerContractDAO() {
        return new CustomerContractDAOImpl();
    }
}
