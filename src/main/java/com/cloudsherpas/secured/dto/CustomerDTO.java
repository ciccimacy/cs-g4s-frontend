package com.cloudsherpas.secured.dto;

import java.util.Date;
import java.util.List;

import com.cloudsherpas.secured.enums.GenderEnum;

public class CustomerDTO {

    private String firstName;
    private String lastName;
    private GenderEnum gender;
    private String phoneNumber;
    private Date birthday;
    private String birthdayStr;
    private String companyName;
    private String email;
    private String streetAddress;
    private String city;
    private String state;
    private double postcode;
    private String country;
    private String customerNumber;
    private List<String> contractNum;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public GenderEnum getGender() {
        return gender;
    }

    public void setGender(final GenderEnum gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(final Date birthday) {
        this.birthday = birthday;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(final String companyName) {
        this.companyName = companyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(final String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public double getPostcode() {
        return postcode;
    }

    public void setPostcode(final double postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(final String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getBirthdayStr() {
        return birthdayStr;
    }

    public void setBirthdayStr(final String birthdayStr) {
        this.birthdayStr = birthdayStr;
    }

    public List<String> getContractNum() {
        return contractNum;
    }

    public void setContractNum(final List<String> contractNum) {
        this.contractNum = contractNum;
    }

}
