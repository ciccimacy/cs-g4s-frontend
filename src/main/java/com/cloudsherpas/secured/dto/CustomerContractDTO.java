package com.cloudsherpas.secured.dto;

import java.util.ArrayList;
import java.util.List;

public class CustomerContractDTO {
    private String customerNumber;
    private List<String> contractNum = new ArrayList<String>();

    public String getCustomerNumber() {
        return customerNumber;
    }
    public void setCustomerNumber(final String customerNumber) {
        this.customerNumber = customerNumber;
    }
    public List<String> getContractNum() {
        return contractNum;
    }
    public void setContractNum(final List<String> contractNum) {
        this.contractNum = contractNum;
    }
}
