package com.cloudsherpas.secured.dto;

public class ContractDTO {

    private String contractNum;
    private String title;
    private String body;

    public String getContractNum() {
        return contractNum;
    }
    public void setContractNum(final String contractNum) {
        this.contractNum = contractNum;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(final String title) {
        this.title = title;
    }
    public String getBody() {
        return body;
    }
    public void setBody(final String body) {
        this.body = body;
    }
}
