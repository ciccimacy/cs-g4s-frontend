package com.cloudsherpas.secured.dto;

public class CustomerContractWebDTO {

    private CustomerDTO customerDTO;
    private CustomerContractDTO customerContractDTO;
    public CustomerDTO getCustomerDTO() {
        return customerDTO;
    }
    public void setCustomerDTO(final CustomerDTO customerDTO) {
        this.customerDTO = customerDTO;
    }
    public CustomerContractDTO getCustomerContractDTO() {
        return customerContractDTO;
    }
    public void setCustomerContractDTO(final CustomerContractDTO customerContractDTO) {
        this.customerContractDTO = customerContractDTO;
    }

}
