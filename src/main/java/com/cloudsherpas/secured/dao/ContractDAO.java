package com.cloudsherpas.secured.dao;

import java.util.List;

import com.cloudsherpas.secured.models.ContractModel;

public interface ContractDAO extends BaseDAO<ContractModel> {
    List<ContractModel> getContractByContractNum(final String contractNum);
}
