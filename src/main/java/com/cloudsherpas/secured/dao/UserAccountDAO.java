package com.cloudsherpas.secured.dao;

import java.util.List;

import com.cloudsherpas.secured.models.UserAccountModel;

public interface UserAccountDAO extends BaseDAO<UserAccountModel> {
    List<UserAccountModel> getUserAccountByEmail(final String email);
    List<UserAccountModel> verifyLogin(final String email, final String password);
}
