package com.cloudsherpas.secured.dao;

import com.cloudsherpas.secured.models.ContractModel;
import com.cloudsherpas.secured.models.CustomerContractModel;
import com.cloudsherpas.secured.models.CustomerModel;
import com.cloudsherpas.secured.models.UserAccountModel;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.impl.translate.opt.joda.JodaTimeTranslators;

public final class DAOManager {

    private static DAOManager self;

    static {
        registerEntities();
    }

    private DAOManager() {
    }

    public static DAOManager getInstance() {
        if (self == null) {
            self = new DAOManager();
        }

        return self;
    }

    private static void registerEntities() {
        ObjectifyService.begin();

        JodaTimeTranslators.add(ObjectifyService.factory());

        ObjectifyService.factory().register(CustomerModel.class);
        ObjectifyService.factory().register(UserAccountModel.class);
        ObjectifyService.factory().register(ContractModel.class);
        ObjectifyService.factory().register(CustomerContractModel.class);
    }

    public Objectify getObjectify() {
        return ObjectifyService.ofy();
    }
}
