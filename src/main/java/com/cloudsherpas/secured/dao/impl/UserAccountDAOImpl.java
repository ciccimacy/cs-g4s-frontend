package com.cloudsherpas.secured.dao.impl;

import java.util.List;

import com.cloudsherpas.secured.dao.DAOManager;
import com.cloudsherpas.secured.dao.UserAccountDAO;
import com.cloudsherpas.secured.models.UserAccountModel;
import com.googlecode.objectify.Objectify;

public class UserAccountDAOImpl extends BaseDAOImpl<UserAccountModel> implements
        UserAccountDAO {
    private static final DAOManager DAO_MANAGER = DAOManager.getInstance();

    public UserAccountDAOImpl() {
        super(UserAccountModel.class);
    }

    @Override
    public List<UserAccountModel> getUserAccountByEmail(final String email) {
        final Objectify ofy = DAO_MANAGER.getObjectify();
        return ofy.load().type(UserAccountModel.class).filter("email", email)
                .list();
    }

    @Override
    public List<UserAccountModel> verifyLogin(final String email, final String password) {
        final Objectify ofy = DAO_MANAGER.getObjectify();
        return ofy.load().type(UserAccountModel.class).filter("email", email)
                .filter("password", password).list();
    }
}
