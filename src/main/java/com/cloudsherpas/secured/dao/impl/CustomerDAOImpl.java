package com.cloudsherpas.secured.dao.impl;

import java.util.List;

import com.cloudsherpas.secured.dao.CustomerDAO;
import com.cloudsherpas.secured.dao.DAOManager;
import com.cloudsherpas.secured.models.CustomerModel;
import com.googlecode.objectify.Objectify;

public class CustomerDAOImpl extends BaseDAOImpl<CustomerModel> implements
        CustomerDAO {
    private static final DAOManager DAO_MANAGER = DAOManager.getInstance();

    public CustomerDAOImpl() {
        super(CustomerModel.class);
    }

    @Override
    public List<CustomerModel> getRecordByCustomerNumber(final String customerNumber) {
        final Objectify ofy = DAO_MANAGER.getObjectify();
        return ofy.load().type(CustomerModel.class)
                .filter("customerNumber", customerNumber).list();
    }

    @Override
    public List<CustomerModel> getRecordByEmail(final String email) {
        final Objectify ofy = DAO_MANAGER.getObjectify();
        return ofy.load().type(CustomerModel.class)
                .filter("email", email).list();
    }
}
