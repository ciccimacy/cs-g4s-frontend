package com.cloudsherpas.secured.dao.impl;

import java.util.List;

import com.cloudsherpas.secured.constants.CustomerContractDAO;
import com.cloudsherpas.secured.dao.DAOManager;
import com.cloudsherpas.secured.models.CustomerContractModel;
import com.googlecode.objectify.Objectify;

public class CustomerContractDAOImpl extends BaseDAOImpl<CustomerContractModel>
        implements CustomerContractDAO {

    private static final DAOManager DAO_MANAGER = DAOManager.getInstance();

    public CustomerContractDAOImpl() {
        super(CustomerContractModel.class);
    }

    @Override
    public List<CustomerContractModel> getCustomerAndContractByCustomerNumber(
            final String customerNumber) {
        final Objectify ofy = DAO_MANAGER.getObjectify();
        return ofy.load().type(CustomerContractModel.class)
                .filter("customerNumber", customerNumber).list();
    }

}
