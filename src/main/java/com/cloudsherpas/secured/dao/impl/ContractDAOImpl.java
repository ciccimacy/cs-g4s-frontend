package com.cloudsherpas.secured.dao.impl;

import java.util.List;

import com.cloudsherpas.secured.dao.ContractDAO;
import com.cloudsherpas.secured.dao.DAOManager;
import com.cloudsherpas.secured.models.ContractModel;
import com.googlecode.objectify.Objectify;

public class ContractDAOImpl extends BaseDAOImpl<ContractModel> implements
        ContractDAO {

    private static final DAOManager DAO_MANAGER = DAOManager.getInstance();

    public ContractDAOImpl() {
        super(ContractModel.class);
    }

    @Override
    public List<ContractModel> getContractByContractNum(final String contractNum) {
        final Objectify ofy = DAO_MANAGER.getObjectify();
        return ofy.load().type(ContractModel.class)
                .filter("contractNum", contractNum).list();
    }
}
