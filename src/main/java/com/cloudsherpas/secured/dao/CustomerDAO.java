package com.cloudsherpas.secured.dao;

import java.util.List;

import com.cloudsherpas.secured.models.CustomerModel;

public interface CustomerDAO extends BaseDAO<CustomerModel> {
    List<CustomerModel> getRecordByCustomerNumber(final String customerNumber);
    List<CustomerModel> getRecordByEmail(final String email);
}
