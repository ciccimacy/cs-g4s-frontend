package com.cloudsherpas.secured.constants;

import java.util.List;

import com.cloudsherpas.secured.dao.BaseDAO;
import com.cloudsherpas.secured.models.CustomerContractModel;

public interface CustomerContractDAO extends BaseDAO<CustomerContractModel> {

    List<CustomerContractModel> getCustomerAndContractByCustomerNumber(final String customerNumber);
}
