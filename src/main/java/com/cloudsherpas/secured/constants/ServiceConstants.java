package com.cloudsherpas.secured.constants;

public final class ServiceConstants {
    public static final String CUSTOMER_SERVICE = "customerService";
    public static final String USER_SERVICE = "userAccountService";
    public static final String CONTRACT_SERVICE = "contractService";
    public static final String CUSTOMER_CONTRACT_SERVICE = "customerContractService";

    private ServiceConstants() {
    }
}
