package com.cloudsherpas.secured.constants;

public final class DAOConstants {

    public static final String CUSTOMER_DAO = "customerDAO";
    public static final String USER_DAO = "userDAO";
    public static final String CONTRACT_DAO = "contractDAO";
    public static final String CUSTOMER_CONTRACT_DAO = "customerContractDAO";

    private DAOConstants() {
    }
}
