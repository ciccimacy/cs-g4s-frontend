package com.cloudsherpas.secured.constants;

public final class ResourceConstants {
    public static final String API_NAME = "secured";

    public static final String API_VERSION = "v1";

    public static final String API_NAMESPACE_OWNER_DOMAIN = "cloudsherpas.com";

    public static final String API_NAMESPACE_OWNER_NAME = "CloudSherpas";

    public static final String API_DESCRIPTION = "Sample API for Javelin Secured";

    private ResourceConstants() {

    }
}
