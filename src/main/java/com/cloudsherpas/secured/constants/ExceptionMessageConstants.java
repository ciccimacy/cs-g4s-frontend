package com.cloudsherpas.secured.constants;

public final class ExceptionMessageConstants {
    public static final String NO_CONTENTS_FOUND = "No records found";
    public static final String INVALID_LOGIN = "Invalid login credentials";
    public static final String PERSIST_FAILED = "Cannot persist data";
    public static final String CUSTOMERNUMBER_REQUIRED = "Customer number is required";
    public static final String CUSTOMER_NOT_EXISTING = "Customer is not yet existing";
    public static final String DELETION_FAILED = "Cannot delete data";
    public static final String NULL_DATE = "Date is required";
    public static final String INVALID_DATE = "Date is invalid";
    public static final String DUPLICATE_EMAIL = "Duplicate Email";
    public static final String EMAIL_REQUIRED = "Email is required";
    public static final String CONTRACTNUMBER_REQUIRED = "Contract number is required";
    public static final String DUPLICATE_CONTRACTNUMBER = "Duplicate Contract number";
    public static final String DUPLICATE_CUSTOMERNUMBER = "Duplicate Customer number";
    public static final String UNABLE_TO_UPDATE = "Unable to update data";
    private ExceptionMessageConstants() {
    }
}
