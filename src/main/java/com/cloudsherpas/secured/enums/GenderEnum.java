package com.cloudsherpas.secured.enums;

public enum GenderEnum {
    FEMALE("Female"),
    MALE("Male");

    private final String description;

    private GenderEnum(final String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
