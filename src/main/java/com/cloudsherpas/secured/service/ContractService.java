package com.cloudsherpas.secured.service;

import java.util.List;

import com.cloudsherpas.secured.dto.ContractDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;

public interface ContractService {
    List<ContractDTO> getAllContracts() throws SecuredException;
    Long addContract(final ContractDTO contract) throws SecuredException;
    void deleteContractByContractNum(final String contractNum) throws SecuredException;
    Long updateContract(final ContractDTO contract) throws SecuredException;
    ContractDTO getContractByContractNum(final String contractNum) throws SecuredException;
}
