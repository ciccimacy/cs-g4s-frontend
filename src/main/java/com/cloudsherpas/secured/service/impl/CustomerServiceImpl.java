package com.cloudsherpas.secured.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

import com.cloudsherpas.secured.constants.DAOConstants;
import com.cloudsherpas.secured.constants.ExceptionMessageConstants;
import com.cloudsherpas.secured.constants.ServiceConstants;
import com.cloudsherpas.secured.dao.CustomerDAO;
import com.cloudsherpas.secured.dto.CustomerContractWebDTO;
import com.cloudsherpas.secured.dto.CustomerDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;
import com.cloudsherpas.secured.models.CustomerModel;
import com.cloudsherpas.secured.service.CustomerContractService;
import com.cloudsherpas.secured.service.CustomerService;

public class CustomerServiceImpl implements CustomerService {

    @Autowired
    @Qualifier(DAOConstants.CUSTOMER_DAO)
    @Lazy
    private CustomerDAO customerDAO;

    @Autowired
    @Qualifier(ServiceConstants.CUSTOMER_CONTRACT_SERVICE)
    @Lazy
    private CustomerContractService customerContractService;

    private ModelMapper modelMapper;

    public CustomerServiceImpl() {
        modelMapper = new ModelMapper();
    }

    @Override
    public List<CustomerDTO> getAllCustomer() throws SecuredException {
        List<CustomerModel> modelList = customerDAO.getAll();
        List<CustomerDTO> dtoList = new ArrayList<CustomerDTO>();

        for (CustomerModel model : modelList) {
            dtoList.add(modelMapper.map(model, CustomerDTO.class));
        }
        return dtoList;
    }

    @Override
    public Long addCustomer(final CustomerDTO customer) throws SecuredException {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

        if (customer.getBirthdayStr() != null) {
            try {
                customer.setBirthday(format.parse(customer.getBirthdayStr()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (customer.getEmail() == null) {
            throw new SecuredException(ExceptionMessageConstants.EMAIL_REQUIRED);
        }

        List<CustomerModel> resultList = customerDAO
                .getRecordByEmail(customer.getEmail());

        if (resultList.size() != 0) {
            throw new SecuredException(
                    ExceptionMessageConstants.DUPLICATE_EMAIL);
        }

        CustomerModel model = modelMapper.map(customer, CustomerModel.class);
        Long key = customerDAO.put(model);
        if (key == null) {
            throw new SecuredException(ExceptionMessageConstants.PERSIST_FAILED);
        }
        return key;
    }

    @Override
    public void deleteCustomerByCustomerNumber(final String customerNumber)
            throws SecuredException {
        if (customerNumber == null) {
            throw new SecuredException(
                    ExceptionMessageConstants.DELETION_FAILED);
        }
        List<CustomerModel> resultList = customerDAO
                .getRecordByCustomerNumber(customerNumber);

        if (resultList.size() == 0) {
            throw new SecuredException(
                    ExceptionMessageConstants.NO_CONTENTS_FOUND);
        }

        for (CustomerModel customer : resultList) {
            customerDAO.delete(customer.getId());
        }
    }

    @Override
    public Long updateCustomer(final CustomerDTO customer)
            throws SecuredException {
        deleteCustomerByCustomerNumber(customer.getCustomerNumber());
        Long key = addCustomer(customer);
        return key;
    }

    @Override
    public CustomerDTO getCustomerByID(final String customerNumber)
            throws SecuredException {
        if (customerNumber == null) {
            throw new SecuredException(
                    ExceptionMessageConstants.CUSTOMERNUMBER_REQUIRED);
        }
        List<CustomerModel> resultList = customerDAO
                .getRecordByCustomerNumber(customerNumber);

        if (resultList.size() == 0) {
            throw new SecuredException(
                    ExceptionMessageConstants.NO_CONTENTS_FOUND);
        }
        return modelMapper.map(resultList.get(0), CustomerDTO.class);
    }

    @Override
    public Long addCustomerContract(
            final CustomerContractWebDTO customerContractWebDTO)
            throws SecuredException {

        if (customerContractWebDTO == null
                || customerContractWebDTO.getCustomerDTO() == null) {
            throw new SecuredException(ExceptionMessageConstants.PERSIST_FAILED);
        }

        // CSOFF: MagicNumber
        customerContractWebDTO.getCustomerDTO().setCustomerNumber(
                "REF" + (int) (Math.random() * 100000000));
        customerContractWebDTO.getCustomerContractDTO().setCustomerNumber(
                customerContractWebDTO.getCustomerDTO().getCustomerNumber());
        // CSON: MagicNumber

        if (customerContractWebDTO.getCustomerContractDTO() != null) {
            customerContractService.addCustomerContracts(customerContractWebDTO
                    .getCustomerContractDTO());
        }
        return addCustomer(customerContractWebDTO.getCustomerDTO());
    }

    @Override
    public void deleteCustomerContractByCustomerNumber(
            final String customerNumber) throws SecuredException {
        deleteCustomerByCustomerNumber(customerNumber);
        customerContractService
                .deleteCustomerContractsByCustomerNumber(customerNumber);
    }

    @Override
    public Long updateCustomerContract(
            final CustomerContractWebDTO customerContractWebDTO)
            throws SecuredException {

        if (customerContractWebDTO == null
                || customerContractWebDTO.getCustomerDTO() == null) {
            throw new SecuredException(
                    ExceptionMessageConstants.UNABLE_TO_UPDATE);
        }

        if (customerContractWebDTO.getCustomerContractDTO() != null) {
            customerContractService
                    .updateCustomerContracts(customerContractWebDTO
                            .getCustomerContractDTO());
        }
        return updateCustomer(customerContractWebDTO.getCustomerDTO());
    }

    @Override
    public CustomerContractWebDTO getCustomerContract(
            final String customerNumber) throws SecuredException {
        CustomerContractWebDTO response = new CustomerContractWebDTO();
        response.setCustomerDTO(getCustomerByID(customerNumber));
        response.setCustomerContractDTO(customerContractService
                .getContractsForCustomer(customerNumber));
        return response;
    }

}
