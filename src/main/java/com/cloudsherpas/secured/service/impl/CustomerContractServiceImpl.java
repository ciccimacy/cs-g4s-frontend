package com.cloudsherpas.secured.service.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

import com.cloudsherpas.secured.constants.CustomerContractDAO;
import com.cloudsherpas.secured.constants.DAOConstants;
import com.cloudsherpas.secured.constants.ExceptionMessageConstants;
import com.cloudsherpas.secured.dto.CustomerContractDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;
import com.cloudsherpas.secured.models.CustomerContractModel;
import com.cloudsherpas.secured.service.CustomerContractService;

public class CustomerContractServiceImpl implements CustomerContractService {

    @Autowired
    @Qualifier(DAOConstants.CUSTOMER_CONTRACT_DAO)
    @Lazy
    private CustomerContractDAO customerContractDAO;

    private ModelMapper modelMapper;

    public CustomerContractServiceImpl() {
        modelMapper = new ModelMapper();
    }

    @Override
    public Long addCustomerContracts(final CustomerContractDTO customerContractDTO)
            throws SecuredException {
        if (customerContractDTO.getContractNum() == null) {
            throw new SecuredException(
                    ExceptionMessageConstants.CUSTOMERNUMBER_REQUIRED);
        }

        List<CustomerContractModel> resultList = customerContractDAO
                .getCustomerAndContractByCustomerNumber(customerContractDTO
                        .getCustomerNumber());

        if (resultList.size() != 0) {
            throw new SecuredException(
                    ExceptionMessageConstants.DUPLICATE_CUSTOMERNUMBER);
        }

        CustomerContractModel model = modelMapper.map(customerContractDTO,
                CustomerContractModel.class);
        Long key = customerContractDAO.put(model);
        if (key == null) {
            throw new SecuredException(ExceptionMessageConstants.PERSIST_FAILED);
        }
        return key;
    }

    @Override
    public void deleteCustomerContractsByCustomerNumber(final String customerNumber)
            throws SecuredException {
        if (customerNumber == null) {
            throw new SecuredException(
                    ExceptionMessageConstants.DELETION_FAILED);
        }
        List<CustomerContractModel> resultList = customerContractDAO
                .getCustomerAndContractByCustomerNumber(customerNumber);

        if (resultList.size() == 0) {
            throw new SecuredException(
                    ExceptionMessageConstants.NO_CONTENTS_FOUND);
        }

        for (CustomerContractModel customerContract : resultList) {
            customerContractDAO.delete(customerContract.getId());
        }

    }

    @Override
    public Long updateCustomerContracts(final CustomerContractDTO customerContractDTO)
            throws SecuredException {
        deleteCustomerContractsByCustomerNumber(customerContractDTO
                .getCustomerNumber());
        Long key = addCustomerContracts(customerContractDTO);
        return key;
    }

    @Override
    public CustomerContractDTO getContractsForCustomer(final String customerNumber)
            throws SecuredException {
        if (customerNumber == null) {
            throw new SecuredException(
                    ExceptionMessageConstants.CUSTOMERNUMBER_REQUIRED);
        }
        List<CustomerContractModel> resultList = customerContractDAO
                .getCustomerAndContractByCustomerNumber(customerNumber);

        if (resultList.size() == 0) {
            throw new SecuredException(
                    ExceptionMessageConstants.NO_CONTENTS_FOUND);
        }
        return modelMapper.map(resultList.get(0), CustomerContractDTO.class);
    }

}
