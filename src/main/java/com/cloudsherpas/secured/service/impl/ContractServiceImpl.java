package com.cloudsherpas.secured.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

import com.cloudsherpas.secured.constants.DAOConstants;
import com.cloudsherpas.secured.constants.ExceptionMessageConstants;
import com.cloudsherpas.secured.dao.ContractDAO;
import com.cloudsherpas.secured.dto.ContractDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;
import com.cloudsherpas.secured.models.ContractModel;
import com.cloudsherpas.secured.service.ContractService;

public class ContractServiceImpl implements ContractService {

    @Autowired
    @Qualifier(DAOConstants.CONTRACT_DAO)
    @Lazy
    private ContractDAO contractDAO;
    private ModelMapper modelMapper;

    public ContractServiceImpl() {
        modelMapper = new ModelMapper();
    }

    @Override
    public List<ContractDTO> getAllContracts() throws SecuredException {
        List<ContractModel> modelList = contractDAO.getAll();
        List<ContractDTO> dtoList = new ArrayList<ContractDTO>();

        for (ContractModel model : modelList) {
            dtoList.add(modelMapper.map(model, ContractDTO.class));
        }
        return dtoList;
    }

    @Override
    public Long addContract(final ContractDTO contract) throws SecuredException {
        if (contract.getContractNum() == null) {
            throw new SecuredException(
                    ExceptionMessageConstants.CONTRACTNUMBER_REQUIRED);
        }

        List<ContractModel> resultList = contractDAO
                .getContractByContractNum(contract.getContractNum());

        if (resultList.size() != 0) {
            throw new SecuredException(
                    ExceptionMessageConstants.DUPLICATE_CONTRACTNUMBER);
        }

        ContractModel model = modelMapper.map(contract, ContractModel.class);
        Long key = contractDAO.put(model);
        if (key == null) {
            throw new SecuredException(ExceptionMessageConstants.PERSIST_FAILED);
        }
        return key;
    }

    @Override
    public void deleteContractByContractNum(final String contractNum)
            throws SecuredException {
        if (contractNum == null) {
            throw new SecuredException(
                    ExceptionMessageConstants.DELETION_FAILED);
        }
        List<ContractModel> resultList = contractDAO
                .getContractByContractNum(contractNum);

        if (resultList.size() == 0) {
            throw new SecuredException(
                    ExceptionMessageConstants.NO_CONTENTS_FOUND);
        }

        for (ContractModel customer : resultList) {
            contractDAO.delete(customer.getId());
        }

    }

    @Override
    public Long updateContract(final ContractDTO contract) throws SecuredException {
        deleteContractByContractNum(contract.getContractNum());
        Long key = addContract(contract);
        return key;
    }

    @Override
    public ContractDTO getContractByContractNum(final String contractNum)
            throws SecuredException {
        if (contractNum == null) {
            throw new SecuredException(
                    ExceptionMessageConstants.CUSTOMERNUMBER_REQUIRED);
        }
        List<ContractModel> resultList = contractDAO
                .getContractByContractNum(contractNum);

        if (resultList.size() == 0) {
            throw new SecuredException(
                    ExceptionMessageConstants.NO_CONTENTS_FOUND);
        }
        return modelMapper.map(resultList.get(0),
                ContractDTO.class);
    }

}
