package com.cloudsherpas.secured.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

import com.cloudsherpas.secured.constants.DAOConstants;
import com.cloudsherpas.secured.constants.ExceptionMessageConstants;
import com.cloudsherpas.secured.dao.UserAccountDAO;
import com.cloudsherpas.secured.dto.UserAccountDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;
import com.cloudsherpas.secured.models.UserAccountModel;
import com.cloudsherpas.secured.service.UserAccountService;
import com.google.appengine.repackaged.com.google.api.client.util.Base64;

public class UserAccountServiceImpl implements UserAccountService {

    @Autowired
    @Qualifier(DAOConstants.USER_DAO)
    @Lazy
    private UserAccountDAO userAccountDAO;
    private ModelMapper modelMapper;

    public UserAccountServiceImpl() {
        modelMapper = new ModelMapper();
    }

    @Override
    public List<UserAccountDTO> getAllUserAccount() {
        List<UserAccountModel> modelList = userAccountDAO.getAll();
        List<UserAccountDTO> dtoList = new ArrayList<UserAccountDTO>();

        for (UserAccountModel model : modelList) {
            model.setPassword(decryptPassword(model.getPassword()));
            dtoList.add(modelMapper.map(model, UserAccountDTO.class));
        }
        return dtoList;
    }

    @Override
    public Long addUserAccount(final UserAccountDTO user)
            throws SecuredException {

        if (user.getEmail() == null) {
            throw new SecuredException(ExceptionMessageConstants.EMAIL_REQUIRED);
        }

        List<UserAccountModel> resultList = userAccountDAO
                .getUserAccountByEmail(user.getEmail());

        if (resultList.size() != 0) {
            throw new SecuredException(
                    ExceptionMessageConstants.DUPLICATE_EMAIL);
        }

        user.setPassword(encryptPassword(user.getPassword()));
        UserAccountModel model = modelMapper.map(user, UserAccountModel.class);
        Long key = userAccountDAO.put(model);
        if (key == null) {
            throw new SecuredException(ExceptionMessageConstants.PERSIST_FAILED);
        }
        return key;
    }

    @Override
    public void deleteUserAccountByEmail(final String email)
            throws SecuredException {
        if (email == null) {
            throw new SecuredException(
                    ExceptionMessageConstants.DELETION_FAILED);
        }
        List<UserAccountModel> resultList = userAccountDAO
                .getUserAccountByEmail(email);

        if (resultList.size() == 0) {
            throw new SecuredException(
                    ExceptionMessageConstants.NO_CONTENTS_FOUND);
        }

        for (UserAccountModel userAccount : resultList) {
            userAccountDAO.delete(userAccount.getId());
        }
    }

    @Override
    public Long updateUserAccount(final UserAccountDTO userAccount)
            throws SecuredException {
        deleteUserAccountByEmail(userAccount.getEmail());
        Long key = addUserAccount(userAccount);
        return key;
    }

    @Override
    public UserAccountDTO getUserAccountByEmail(final String email)
            throws SecuredException {
        if (email == null) {
            throw new SecuredException(ExceptionMessageConstants.EMAIL_REQUIRED);
        }

        List<UserAccountModel> resultList = userAccountDAO
                .getUserAccountByEmail(email);

        if (resultList.size() == 0) {
            throw new SecuredException(
                    ExceptionMessageConstants.NO_CONTENTS_FOUND);
        }
        resultList.get(0).setPassword(
                decryptPassword(resultList.get(0).getPassword()));
        return modelMapper.map(resultList.get(0), UserAccountDTO.class);
    }

    @Override
    public UserAccountDTO verifyLogin(final String email, final String password)
            throws SecuredException {
        if (email == null || password == null) {
            throw new SecuredException(ExceptionMessageConstants.INVALID_LOGIN);
        }

        List<UserAccountModel> resultList = userAccountDAO.verifyLogin(email,
                encryptPassword(password));

        if (resultList.size() == 0) {
            throw new SecuredException(ExceptionMessageConstants.INVALID_LOGIN);
        }
        resultList.get(0).setPassword(
                decryptPassword(resultList.get(0).getPassword()));
        return modelMapper.map(resultList.get(0), UserAccountDTO.class);
    }

    private String encryptPassword(final String password)
            throws SecuredException {
        String encryptedText = "";
        if (password != null) {
            encryptedText = new String(Base64.encodeBase64(password.getBytes()));
        }
        return encryptedText;
    }

    private String decryptPassword(final String encryptedPassword) {
        byte[] byteArray = null;
        String decodedString = "";
        if (encryptedPassword != null) {
            byteArray = Base64.decodeBase64(encryptedPassword.getBytes());
            decodedString = new String(byteArray);
        }
        return decodedString;
    }

}
