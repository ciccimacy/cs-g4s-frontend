package com.cloudsherpas.secured.service;

import java.util.List;

import com.cloudsherpas.secured.dto.UserAccountDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;

public interface UserAccountService {
    List<UserAccountDTO> getAllUserAccount();
    Long addUserAccount(final UserAccountDTO user) throws SecuredException;
    void deleteUserAccountByEmail(final String email) throws SecuredException;
    Long updateUserAccount(final UserAccountDTO userAccount) throws SecuredException;
    UserAccountDTO getUserAccountByEmail(final String email) throws SecuredException;
    UserAccountDTO verifyLogin(final String email, final String password) throws SecuredException;
}
