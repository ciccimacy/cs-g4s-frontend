package com.cloudsherpas.secured.service;

import java.util.List;

import com.cloudsherpas.secured.dto.CustomerContractWebDTO;
import com.cloudsherpas.secured.dto.CustomerDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;

public interface CustomerService {
    List<CustomerDTO> getAllCustomer() throws SecuredException;

    Long addCustomer(final CustomerDTO customer) throws SecuredException;

    void deleteCustomerByCustomerNumber(final String customerNumber)
            throws SecuredException;

    Long updateCustomer(final CustomerDTO customer) throws SecuredException;

    CustomerDTO getCustomerByID(final String customerNumber)
            throws SecuredException;

    Long addCustomerContract(final CustomerContractWebDTO customerContractWebDTO)
            throws SecuredException;

    void deleteCustomerContractByCustomerNumber(final String customerNumber)
            throws SecuredException;

    Long updateCustomerContract(final CustomerContractWebDTO customerContractWebDTO)
            throws SecuredException;

    CustomerContractWebDTO getCustomerContract(final String customerNumber)
            throws SecuredException;

}
