package com.cloudsherpas.secured.service;

import com.cloudsherpas.secured.dto.CustomerContractDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;

public interface CustomerContractService {

    Long addCustomerContracts(final CustomerContractDTO customerContractDTO) throws SecuredException;
    void deleteCustomerContractsByCustomerNumber(final String customerNumber) throws SecuredException;
    Long updateCustomerContracts(final CustomerContractDTO customerContractDTO) throws SecuredException;
    CustomerContractDTO getContractsForCustomer(String customerNumber) throws SecuredException;


}
