package com.cloudsherpas.secured.exceptions;

import com.google.api.server.spi.ServiceException;

//CSOFF: MagicNumber
public class SecuredException extends ServiceException {
    public SecuredException(final String message) {
        super(408, message);
    }
}
// CSON: MagicNumber

