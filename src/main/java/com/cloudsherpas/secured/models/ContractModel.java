package com.cloudsherpas.secured.models;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Index
public class ContractModel {

    @Id
    private Long id;
    private String contractNum;
    private String title;
    private String body;
    public Long getId() {
        return id;
    }
    public void setId(final Long id) {
        this.id = id;
    }
    public String getContractNum() {
        return contractNum;
    }
    public void setContractNum(final String contractNum) {
        this.contractNum = contractNum;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(final String title) {
        this.title = title;
    }
    public String getBody() {
        return body;
    }
    public void setBody(final String body) {
        this.body = body;
    }
}
