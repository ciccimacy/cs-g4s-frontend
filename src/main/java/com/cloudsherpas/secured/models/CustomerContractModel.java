package com.cloudsherpas.secured.models;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Index
public class CustomerContractModel {

    @Id
    private Long id;
    private String customerNumber;
    private List<String> contractNum = new ArrayList<String>();

    public Long getId() {
        return id;
    }
    public void setId(final Long id) {
        this.id = id;
    }
    public String getCustomerNumber() {
        return customerNumber;
    }
    public void setCustomerNumber(final String customerNumber) {
        this.customerNumber = customerNumber;
    }
    public List<String> getContractNum() {
        return contractNum;
    }
    public void setContractNum(final List<String> contractNum) {
        this.contractNum = contractNum;
    }

}
