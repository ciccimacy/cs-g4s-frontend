package com.cloudsherpas.secured.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

import com.cloudsherpas.secured.constants.ResourceConstants;
import com.cloudsherpas.secured.constants.ServiceConstants;
import com.cloudsherpas.secured.dto.CustomerContractWebDTO;
import com.cloudsherpas.secured.dto.CustomerDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;
import com.cloudsherpas.secured.service.CustomerService;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;

@Api(name = ResourceConstants.API_NAME,
    version = ResourceConstants.API_VERSION,
    namespace = @ApiNamespace(ownerDomain = ResourceConstants.API_NAMESPACE_OWNER_DOMAIN,
    ownerName = ResourceConstants.API_NAMESPACE_OWNER_NAME),
    description = ResourceConstants.API_DESCRIPTION)
public class CustomerResource {

    private static final String KEY = "key";
    @Autowired
    @Qualifier(ServiceConstants.CUSTOMER_SERVICE)
    @Lazy
    private CustomerService customerService;

    /**
     * This api is responsible for persisting new Customer
     * @return response
     */
//    @ApiMethod(name = "customer.create", path = "customer/create", httpMethod = ApiMethod.HttpMethod.POST)
//    public Map<String, Long> addCustomer(final CustomerDTO customerDTO)
//            throws SecuredException {
//        //CSOFF: MagicNumber
//        customerDTO.setCustomerNumber("REF" + (int) (Math.random() * 100000000));
//        // CSON: MagicNumber
//        Long key = customerService.addCustomer(customerDTO);
//        final Map<String, Long> result = new HashMap<>();
//        if (key != null) {
//            result.put(KEY, key);
//        }
//        return result;
//    }

    /**
     * This api is responsible for getting all the Customer Record
     * @return response
     */
    @ApiMethod(name = "customer.list", path = "customer/list", httpMethod = ApiMethod.HttpMethod.GET)
    public List<CustomerDTO> getAllCustomer() throws SecuredException {
        return customerService.getAllCustomer();
    }

//    /**
//     * This api is responsible for deleting a Customer Record via customer
//     * number
//     * @throws SecuredException
//     */
//    @ApiMethod(name = "customer.delete", path = "customer/delete", httpMethod = ApiMethod.HttpMethod.DELETE)
//    public void deleteCustomer(
//            @Named("customerNumber") final String customerNumber)
//            throws SecuredException {
//        customerService.deleteCustomerByCustomerNumber(customerNumber);
//    }

//    /**
//     * This api is responsible for updating Customer Record
//     * @return response
//     */
//    @ApiMethod(name = "customer.update", path = "customer/update", httpMethod = ApiMethod.HttpMethod.PUT)
//    public Map<String, Long> updateCustomer(final CustomerDTO customerDTO)
//            throws SecuredException {
//        Long key = customerService.updateCustomer(customerDTO);
//        final Map<String, Long> result = new HashMap<>();
//        if (key != null) {
//            result.put(KEY, key);
//        }
//        return result;
//    }

//    /**
//     * This api is responsible getting an Existing Customer Record via customer number
//     * @return response
//     */
//    @ApiMethod(name = "customer.get", path = "customer/get", httpMethod = ApiMethod.HttpMethod.GET)
//    public CustomerDTO getCustomer(@Named("customerNumber") final String customerNumber)
//            throws SecuredException {
//        return customerService.getCustomerByID(customerNumber);
//    }

    /**
     * This api is responsible for persisting new Customer and Contract
     * @return response
     */
    @ApiMethod(name = "customercontract.create", path = "customercontract/create", httpMethod = ApiMethod.HttpMethod.POST)
    public Map<String, Long> addCustomerContract(final CustomerContractWebDTO customerContractWebDTO)
            throws SecuredException {
        Long key = customerService.addCustomerContract(customerContractWebDTO);
        final Map<String, Long> result = new HashMap<>();
        if (key != null) {
            result.put(KEY, key);
        }
        return result;
    }

    /**
     * This api is responsible for getting all the Customer and Contract Record
     * @return response
     */
    @ApiMethod(name = "customercontract.get", path = "customercontract/get", httpMethod = ApiMethod.HttpMethod.GET)
    public CustomerContractWebDTO getCustomerContract(@Named("customerNumber") final String customerNumber) throws SecuredException {
        return customerService.getCustomerContract(customerNumber);
    }

    /**
     * This api is responsible for deleting a Customer and Contract Record via customer
     * number
     * @throws SecuredException
     */
    @ApiMethod(name = "customercontract.delete", path = "customercontract/delete", httpMethod = ApiMethod.HttpMethod.DELETE)
    public void deleteCustomerContractByCustomerNumber(
            @Named("customerNumber") final String customerNumber)
            throws SecuredException {
        customerService.deleteCustomerContractByCustomerNumber(customerNumber);
    }

    /**
     * This api is responsible for updating Customer and Contract Record
     * @return response
     */
    @ApiMethod(name = "customercontract.update", path = "customercontract/update", httpMethod = ApiMethod.HttpMethod.PUT)
    public Map<String, Long> updateCustomerContract(final CustomerContractWebDTO customerContractWebDTO)
            throws SecuredException {
        Long key = customerService.updateCustomerContract(customerContractWebDTO);
        final Map<String, Long> result = new HashMap<>();
        if (key != null) {
            result.put(KEY, key);
        }
        return result;
    }


}
