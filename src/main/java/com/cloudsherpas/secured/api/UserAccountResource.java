package com.cloudsherpas.secured.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

import com.cloudsherpas.secured.constants.ResourceConstants;
import com.cloudsherpas.secured.constants.ServiceConstants;
import com.cloudsherpas.secured.dto.UserAccountDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;
import com.cloudsherpas.secured.service.UserAccountService;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;

@Api(name = ResourceConstants.API_NAME,
    version = ResourceConstants.API_VERSION,
    namespace = @ApiNamespace(ownerDomain = ResourceConstants.API_NAMESPACE_OWNER_DOMAIN,
    ownerName = ResourceConstants.API_NAMESPACE_OWNER_NAME),
    description = ResourceConstants.API_DESCRIPTION)
public class UserAccountResource {

    @Autowired
    @Qualifier(ServiceConstants.USER_SERVICE)
    @Lazy
    private UserAccountService userAccountService;

    /**
     * This api is responsible for persisting new User Account
     * @return response
     */
    @ApiMethod(name = "useraccount.create", path = "useraccount/create", httpMethod = ApiMethod.HttpMethod.POST)
    public Map<String, Long> addUserAccount(final UserAccountDTO userAccountDTO)
            throws SecuredException {
        // initial password
        userAccountDTO.setPassword("newpassword");
        Long key = userAccountService.addUserAccount(userAccountDTO);
        final Map<String, Long> result = new HashMap<>();
        if (key != null) {
            result.put("key", key);
        }
        return result;
    }

    /**
     * This api is responsible for getting all the User Account
     * @return response
     */
    @ApiMethod(name = "useraccount.list", path = "useraccount/list", httpMethod = ApiMethod.HttpMethod.GET)
    public List<UserAccountDTO> getAllUserAccount() throws SecuredException {
        return userAccountService.getAllUserAccount();
    }

    /**
     * This api is responsible for deleting a User Account via
     * email
     * @throws SecuredException
     */
    @ApiMethod(name = "useraccount.delete", path = "useraccount/delete", httpMethod = ApiMethod.HttpMethod.DELETE)
    public void deleteUserAccount(
            @Named("email") final String email)
            throws SecuredException {
        userAccountService.deleteUserAccountByEmail(email);
    }

    /**
     * This api is responsible for updating User Account
     * @return response
     */
    @ApiMethod(name = "useraccount.update", path = "useraccount/update", httpMethod = ApiMethod.HttpMethod.PUT)
    public Map<String, Long> updateUserAccount(final UserAccountDTO userAccountDTO)
            throws SecuredException {
        Long key = userAccountService.updateUserAccount(userAccountDTO);
        final Map<String, Long> result = new HashMap<>();
        if (key != null) {
            result.put("key", key);
        }
        return result;
    }

    /**
     * This api is responsible getting an Existing User Account
     * via email
     * @return response
     */
    @ApiMethod(name = "useraccount.get", path = "useraccount/get", httpMethod = ApiMethod.HttpMethod.GET)
    public UserAccountDTO getUserAccountByEmail(@Named("email") final String email)
            throws SecuredException {
        return userAccountService.getUserAccountByEmail(email);
    }

    /**
     * This api is responsible verifying user login
     * via email
     * @return response
     */
    @ApiMethod(name = "useraccount.verify", path = "useraccount/verify", httpMethod = ApiMethod.HttpMethod.GET)
    public UserAccountDTO verifyLogin(@Named("email") final String email, @Named("password") final String password)
            throws SecuredException {
        return userAccountService.verifyLogin(email, password);
    }
}
