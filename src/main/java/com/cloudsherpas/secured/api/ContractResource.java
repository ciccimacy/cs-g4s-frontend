package com.cloudsherpas.secured.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

import com.cloudsherpas.secured.constants.ResourceConstants;
import com.cloudsherpas.secured.constants.ServiceConstants;
import com.cloudsherpas.secured.dto.ContractDTO;
import com.cloudsherpas.secured.exceptions.SecuredException;
import com.cloudsherpas.secured.service.ContractService;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;



@Api(name = ResourceConstants.API_NAME,
    version = ResourceConstants.API_VERSION,
    namespace = @ApiNamespace(ownerDomain = ResourceConstants.API_NAMESPACE_OWNER_DOMAIN,
    ownerName = ResourceConstants.API_NAMESPACE_OWNER_NAME),
    description = ResourceConstants.API_DESCRIPTION)
public class ContractResource {

    private static final AtomicInteger COUNT = new AtomicInteger(0);

    @Autowired
    @Qualifier(ServiceConstants.CONTRACT_SERVICE)
    @Lazy
    private ContractService contractService;

    /**
     * This api is responsible for persisting new Contract
     * @return response
     */
    @ApiMethod(name = "contract.create", path = "contract/create", httpMethod = ApiMethod.HttpMethod.POST)
    public Map<String, Long> addContract(final ContractDTO contractDTO)
            throws SecuredException {
        //CSOFF: MagicNumber
        contractDTO.setContractNum("CONTRACT" + (int) (Math.random() * 100000000));
        // CSON: MagicNumber
        Long key = contractService.addContract(contractDTO);
        final Map<String, Long> result = new HashMap<>();
        if (key != null) {
            result.put("key", key);
        }
        return result;
    }

    /**
     * This api is responsible for getting all the Contracts
     * @return response
     */
    @ApiMethod(name = "contract.list", path = "contract/list", httpMethod = ApiMethod.HttpMethod.GET)
    public List<ContractDTO> getAllContracts() throws SecuredException {
        return contractService.getAllContracts();
    }

    /**
     * This api is responsible for deleting a Contract via contract number
     * number
     * @throws SecuredException
     */
    @ApiMethod(name = "contract.delete", path = "contract/delete", httpMethod = ApiMethod.HttpMethod.DELETE)
    public void deleteContract(
            @Named("contractNum") final String contractNum)
            throws SecuredException {
        contractService.deleteContractByContractNum(contractNum);
    }

    /**
     * This api is responsible for updating a Contract
     * @return response
     */
    @ApiMethod(name = "contract.update", path = "contract/update", httpMethod = ApiMethod.HttpMethod.PUT)
    public Map<String, Long> updateContract(final ContractDTO contractDTO)
            throws SecuredException {
        Long key = contractService.updateContract(contractDTO);
        final Map<String, Long> result = new HashMap<>();
        if (key != null) {
            result.put("key", key);
        }
        return result;
    }

    /**
     * This api is responsible getting an Existing Contract via contract number
     * @return response
     */
    @ApiMethod(name = "contract.get", path = "contract/get", httpMethod = ApiMethod.HttpMethod.GET)
    public ContractDTO getContractByContractNum(@Named("contractNum") final String contractNum)
            throws SecuredException {
        return contractService.getContractByContractNum(contractNum);
    }
}
