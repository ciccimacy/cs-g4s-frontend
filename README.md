# UI Project Template #

Very basic project template for starting a UI project.

## Requirements ##
* Grunt CLI
* NodeJS
* Bower

## How to initialize: ##

    $ npm install
    $ bower install

## How to run: ##
    $ grunt serve