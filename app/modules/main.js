require.config({
  paths: {
    angular: '../../bower_components/angular/angular',
    'angular-route': '../../bower_components/angular-route/angular-route',
    bootstrap: '../../bower_components/bootstrap/dist/js/bootstrap',
    jquery: '../../bower_components/jquery/dist/jquery',
    'customer-endpoint-service': 'customer/customer.endpoint.service',
    'administration-endpoint-service': 'administration/administration.endpoint.service',
    'contract-endpoint-service': 'contract/contract.endpoint.service',
    'dir-paginate': '../../bower_components/angular-utils-pagination/dirPagination',
    async: '../../bower_components/requirejs-plugins/src/async',
    'angular-google-gapi': '../../bower_components/angular-google-gapi/angular-google-gapi.min',
    moment: '../../bower_components/moment/moment',
    'angular-cookies': '../../bower_components/angular-cookies/angular-cookies',
    angularModalService: '../../bower_components/angular-modal-service/dst/angular-modal-service',
    lodash: '../../bower_components/lodash/lodash',
    jspdf: '../../bower_components/jspdf/dist/jspdf.debug',
    'angular-bootstrap': '../../bower_components/angular-bootstrap/ui-bootstrap-tpls',
    'angular-material-icons': '../../bower_components/angular-material-icons/angular-material-icons.min',
    'angular-mocks': '../../bower_components/angular-mocks/angular-mocks',
    'angular-modal-service': '../../bower_components/angular-modal-service/dst/angular-modal-service',
    'angular-sanitize': '../../bower_components/angular-sanitize/angular-sanitize',
    'angular-ui-select': '../../bower_components/angular-ui-select/dist/select',
    'angular-utils-pagination': '../../bower_components/angular-utils-pagination/dirPagination',
    interact: '../../bower_components/interact/interact',
    depend: '../../bower_components/requirejs-plugins/src/depend',
    font: '../../bower_components/requirejs-plugins/src/font',
    goog: '../../bower_components/requirejs-plugins/src/goog',
    image: '../../bower_components/requirejs-plugins/src/image',
    json: '../../bower_components/requirejs-plugins/src/json',
    mdown: '../../bower_components/requirejs-plugins/src/mdown',
    noext: '../../bower_components/requirejs-plugins/src/noext',
    propertyParser: '../../bower_components/requirejs-plugins/src/propertyParser',
    'Markdown.Converter': '../../bower_components/requirejs-plugins/lib/Markdown.Converter',
    text: '../../bower_components/requirejs-plugins/lib/text'
  },
  shim: {
    bootstrap: {
      deps: [
        'jquery'
      ]
    },
    angular: {
      exports: 'angular'
    },
    'angular-route': [
      'angular'
    ],
    'dir-paginate': {
      deps: [
        'angular'
      ]
    },
    'angular-cookies': [
      'angular'
    ],
    lodash: {
      exports: 'lodash'
    },
    jspdf: {
      exports: 'jspdf'
    },
    angularModalService: {
      exports: 'angularModalService',
      deps:[
      'angular'
    ]
  },
  },
  packages: [

  ],
  waitSeconds: 60
});
require([
  'angular',
  'angular-route',
  'customer/customer.module',
  'navigation/navigation.module',
  'administration/administration.module',
  'contract/contract.module',
  'async!https://apis.google.com/js/client.js!onload'
], function () {
  'use strict';

  bootstrap();
  //Load Google APIs here
  function loadGoogleApis() {
    var apiList = [
      //{name: 'oauth2', version: 'v2'},
      {name: 'secured', version: 'v1', url: 'https://cs-g4s-javelin-secure.appspot.com/_ah/api'}
    ];

    var apisToLoad = apiList.length;
    for (var i = 0; i < apiList.length; i++) {
      var api = apiList[i];
      console.log('Loading ' + api.name + '...');
      gapi.client.load(api.name, api.version, loadedApiCallback, api.url);
    }

    function loadedApiCallback() {
      if (--apisToLoad === 0) {
        console.log("Endpoints Service is ready!");
        angular.bootstrap(document,['customerModule','navigationModule','administrationModule','contractModule']);
      }
    }
  }

  function bootstrap() {
    console.log('Bootstrapping modules...');
    //angular.bootstrap(document,['customerModule','navigationModule']);
    loadGoogleApis();
    console.log('Modules bootstrapped!');
  }
});
