define([
'angular',
'angular-route',
'./contract.config',
'./contract.controller',
'./contract.create.controller',
'./contract.edit.controller',
'./contract.service',
'contract-endpoint-service',
'dir-paginate',
'angularModalService'
], function (angular,angularRoute, config, contractController,contractCreateController,contraEditController,service,contractEndpointService,angularModalService) {
  'use strict';

  angular.module('contractModule', ['ngRoute','angularUtils.directives.dirPagination','angularModalService'])
		  .controller('ContractController', contractController)
      .controller('ContractCreateController', contractCreateController)
      .controller('ContractEditController', contraEditController)
		  .service('contractService', service)
      .service('contractEndpointService', contractEndpointService)
		  .config(config);
});
