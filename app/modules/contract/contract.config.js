define(['require'], function (require) {
  'use strict';

  config.$inject = ['$routeProvider'];

  function config($routeProvider) {
    $routeProvider.when('/contracts', {
      templateUrl: require.toUrl('./contracts.html'),
      controller: 'ContractController',
      controllerAs: 'cont',
      resolve: {
        contracts: ['contractService', function (contractService) {
          var result = contractService.getContracts();
          return result;
        }]
      }
    });
  }

  return config;
});
