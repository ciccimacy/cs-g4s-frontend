define(['require'], function (require) {
  'use strict';

  controller.$inject = ['$route','$scope','contractService'];

  function controller($route,$scope,service) {

    $scope.newcontract =
    {
      title : '',
      body : ''
    };

    $scope.showError = false;
    $scope.showSuccessNotif = false;

    //functions
    $scope.saveNewContract = saveNewContract;
    $scope.clearNewContractFields = clearNewContractFields;

    function saveNewContract() {

      service.addContract($scope.newcontract).then(function (data) {
        if(data == "Failed"){
          $scope.notifValue = 'Unable to save the Contract';
          $scope.showError = true;
          $scope.showSuccessNotif = false;
        }else{
          $scope.notifValue = 'New Contract is saved';
          $scope.newcontract = null;
          $scope.showError = false;
          $scope.showSuccessNotif = true;
          $route.reload();
        }
      });
    }

    function clearNewContractFields() {
      $scope.newcontract = null;
    }
  }



  return controller;
});
