define([], function () {
  'use strict';

  function contractEndpointService ($q) {

    return {
      getContracts : function(){
        var p = $q.defer();
        var contractList = [
          {
            contractNum: "CONTRACT27296245",
            title: "Order 101",
            body: "At vero eos et accusamus et iusto odio dignissimos"
          },
          {
            contractNum: "CONTRACT27296246",
            title: "Order 102",
            body: "At vero eos et accusamus et iusto odio dignissimosur"
          },
          {
            contractNum: "CONTRACT27296244",
            title: "Order 103",
            body: "At vero eos et accusamus et iusto odio dignissimos"
          }
        ];

        p.resolve(contractList);
        return p.promise;
      },
      getExistingContract : function(contractNum){
        var p = $q.defer();
        var contract = {
          "contractNum": "CONTRACT80841550",
          "title": "Order 101",
          "body": "At vero eos et accusamus et iusto odio dignissimos"
        };
        p.resolve(contract);
        return p.promise;
      },
      addContract : function(contract){
        var p = $q.defer();
        var key = 1234567;
        p.resolve(key);
        return p.promise;
      },
      updateContract : function(contract){
        var p = $q.defer();
        var key = 77777;
        p.resolve(key);
        return p.promise;
      }
    };
  }

  contractEndpointService.$inject = ['$q'];

  return contractEndpointService;
});
