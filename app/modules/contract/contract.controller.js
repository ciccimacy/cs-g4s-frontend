define([], function () {
  'use strict';

  controller.$inject = ['$route','ModalService','contracts','contractService'];

  function controller($route,ModalService,contracts,service) {
    var cont = this;
    //data table
    cont.contractList = contracts.result.items;
    cont.sortKey = "";
    cont.reverse =false;

    cont.selectedContract = {};
    cont.toggleContractDetails = false;


    //functions
    cont.showCreateNewContractModal = showCreateNewContractModal;
    cont.removeContract = removeContract;
    cont.viewContractPanel = viewContractPanel;
    cont.closeContractDetailsPanel = closeContractDetailsPanel;
    cont.showEditContractModal = showEditContractModal;
    cont.sort = sort;

    function showCreateNewContractModal() {
      ModalService.showModal({
              templateUrl: 'modules/contract/contract-create-modal.html',
              controller: "ContractCreateController"
          }).then(function(modal) {
              modal.element.modal();
              modal.close.then(function(result) {
              });
          });
    }

    function showEditContractModal() {
      ModalService.showModal({
              templateUrl: 'modules/contract/contract-edit-modal.html',
              controller: "ContractEditController",
              inputs:{
                    selectedContract : cont.selectedContract
              }
          }).then(function(modal) {
              modal.element.modal();
              modal.close.then(function(result) {
              });
          });
    }

    function removeContract(index,data){
      // remove the row specified in index
      cont.contractList.splice( index, 1);
      // if no rows left in the array create a blank array
      if (cont.contractList.length === 0){
        cont.contractList = [];
      }
      service.deleteContract(data.contractNum).then(function (data) {
        if(!data.error){
          // cc.selectedCustomer = {};
          // cc.toggleCustomerDetails = false;
          $route.reload();
        }
        else{
          alert("error");
        }
      });
    }

    function viewContractPanel(contractData){
      service.getExistingContract(contractData.contractNum).then(function (data) {
        if(!data.error){
          cont.selectedContract = data.result;
          cont.toggleContractDetails = true;
        }
        else{
          alert("error");
        }
      });

    }

    function closeContractDetailsPanel() {
      cont.toggleContractDetails = false;
      cont.selectedContract = null;
      $route.reload();
    }

    function sort(keyname) {
      cont.sortKey = keyname;   //set the sortKey to the param passed
      cont.reverse = !cont.reverse; //if true make it false and vice versa
    }


  }

  return controller;
});
