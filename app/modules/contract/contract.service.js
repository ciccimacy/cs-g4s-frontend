define([], function () {
  'use strict';
  var contractService = function service($http, contractEndpointService) {
    return {
      getContracts: function () {

        return contractEndpointService.getContracts().then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      },

      getExistingContract: function (contractNum) {

        return contractEndpointService.getExistingContract(contractNum).then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      },

      addContract: function (contract) {
        return contractEndpointService.addContract(contract).then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      },

      updateContract: function (contract) {
        return contractEndpointService.updateContract(contract).then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      },

      deleteContract: function (contractNum) {
        return contractEndpointService.deleteContract(contractNum).then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      }


    };
  };

  contractService.$inject = ['$http','contractEndpointService'];

  return contractService;
});
