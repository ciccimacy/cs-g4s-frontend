define([], function () {
  'use strict';

  function contractEndpointService ($q) {

    return {
      getContracts : function(){
        var p = $q.defer();
        gapi.client.secured.contract.list()
        .then(function(response){
          console.log("Success: " + response.result);
          p.resolve(response);
        },function(response){
          console.log("Fail: " + response.result);
          p.reject(response);
        });
        return p.promise;
      },
      getExistingContract : function(contractNum){
        var p = $q.defer();
        console.log(contractNum);
        gapi.client.secured.contract.get({'contractNum' : contractNum})
        .then(function(response){
          if(response.error != undefined){
            p.resolve("Failed");
          }
          else{
            p.resolve(response);
          }
        },function(response){
          console.log("Fail: " + response);
          p.reject(response);
        });
        return p.promise;
      },
      addContract : function(contract){
        var p = $q.defer();
        gapi.client.secured.contract.create({resource:contract}).execute(function(response) {
          if(response.error != undefined){
            p.resolve("Failed");
          }
          else{
            p.resolve(response);
          }
        },function(response){
          console.log("Fail: " + response);
          p.reject(response);
        });
        return p.promise;
      },
      updateContract : function(contract){
        var p = $q.defer();
        gapi.client.secured.contract.update({resource:contract}).execute(function(response) {
          console.log("Success: " + response.result);
          if(response.error != undefined){
            p.resolve("Failed");
          }
          else{
            p.resolve(response);
          }
        },function(response){
          console.log("Fail: " + response);
          p.reject("Service Unavailable");
        });
        return p.promise;
      },

      deleteContract : function(contractNum){
        var p = $q.defer();
        gapi.client.secured.contract.delete({'contractNum' : contractNum}).execute(function(response) {
          if(response.error != undefined){
            p.resolve("Failed");
          }
          else{
            p.resolve(response);
          }
        },function(response){
          console.log("Fail: " + response);
          p.reject("Service Unavailable");
        });
        return p.promise;
      }
    };
  }

  contractEndpointService.$inject = ['$q'];

  return contractEndpointService;
});
