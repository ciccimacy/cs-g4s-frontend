define(['require'], function (require) {
  'use strict';

  controller.$inject = ['$route','$scope','contractService','selectedContract'];

  function controller($route,$scope,service,selectedContract) {

    $scope.contractTobeEdited = selectedContract;

    $scope.showError = false;
    $scope.showSuccessNotif = false;

    //functions
    $scope.saveEditContract = saveEditContract;
    $scope.clearEditContractFields = clearEditContractFields;

    function saveEditContract() {

      service.updateContract($scope.contractTobeEdited).then(function (data) {
        if(data == "Failed"){
          $scope.notifValue = 'Unable to edit the Contract';
          $scope.showError = true;
          $scope.showSuccessNotif = false;
        }else{
          $scope.notifValue = 'Modified Contract is now updated';
          $scope.showError = false;
          $scope.showSuccessNotif = true;
          $route.reload();
        }
      });
    }

    function clearEditContractFields() {
      $route.reload();
    }
  }



  return controller;
});
