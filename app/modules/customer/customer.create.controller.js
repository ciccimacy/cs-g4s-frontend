define(['require'], function (require) {
  'use strict';

  controller.$inject = ['$route','$scope','countries','genders','customerService','contracts'];

  function controller($route,$scope,countries,genders,service,contracts) {

    $scope.countries = countries;
    $scope.genders = genders;
    $scope.newCustomer = {
      firstName: '',
      lastName: '',
      gender: '',
      birthdayStr: '',
      companyName: '',
      phoneNumber: '',
      email: '',
      streetAddress: '',
      city: '',
      state: '',
      postcode: '',
      country: '',
    };

    $scope.customerData ={
      customerDTO : $scope.newCustomer,
      customerContractDTO: {
        contractNum: []
      }
    }

    //  $scope.selectedContractNumbers = [];
    $scope.repeatSelectContracts = [""];
    $scope.contractList = contracts.result.items;

    $scope.notifValue = '';
    $scope.showError = false;
    $scope.showSuccessNotif = false;
    $scope.datePattern=/^(0?[1-9]|[12][0-9]|3[01])\-(0?[1-9]|1[012])\-([0-9]{4})$/i;


    //functions
    //$scope.saveNewCustomer = saveNewCustomer;
    $scope.clearNewCustomerFields = clearNewCustomerFields;
    $scope.addAnotherContract = addAnotherContract;
    $scope.removeContractRow = removeContractRow;
    $scope.saveNewCustomerContract = saveNewCustomerContract;

    function saveNewCustomerContract(customerCreateForm) {
      if(customerCreateForm.birthdayStr.$error.pattern){
        $scope.notifValue = 'Date format is incorrect';
        $scope.showError = true;
        $scope.showSuccessNotif = false;
        return "";
      }else if(hasDuplicates($scope.customerData.customerContractDTO.contractNum)){
        $scope.notifValue = 'Duplicate selected contract';
        $scope.showError = true;
        $scope.showSuccessNotif = false;
        return "";
      }

      service.addCustomerContact($scope.customerData).then(function (data) {
        if(data == "Failed"){
          $scope.notifValue = 'Unable to save Customer and Contract Record';
          $scope.showError = true;
          $scope.showSuccessNotif = false;
        }
        else if(data == "503"){
          $scope.notifValue = 'Duplicate Email';
          $scope.showError = true;
          $scope.showSuccessNotif = false;
        }
        else{
          $scope.notifValue = 'Customer and Contract Record is saved';
          $scope.newCustomer = null;
          $scope.customerData = null;
          $scope.showError = false;
          $scope.showSuccessNotif = true;
          $route.reload();
        }
      });
    }

    function clearNewCustomerFields() {
      $scope.newCustomer = null;
      $scope.customerData = null;
      $scope.repeatSelectContracts = [""];
    }

    function addAnotherContract() {
      $scope.repeatSelectContracts.push("");
    }

    function removeContractRow(index,data) {
      $scope.customerData.customerContractDTO.contractNum[index] = null;
      $scope.repeatSelectContracts.splice( index, 1);
      // if no rows left in the array create a blank array
      if ($scope.repeatSelectContracts.length === 0){
        $scope.repeatSelectContracts = [];
      }
    }

    function hasDuplicates(a) {
      return _.uniq(a).length !== a.length;
    }
  }

  return controller;
});
