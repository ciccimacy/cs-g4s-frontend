define(['moment'], function (moment) {
  'use strict';

  controller.$inject = ['$route','customerDetails','countries','customerService','ModalService','contracts','contractService'];

  function controller($route,customerDetails,countries,service,ModalService,contracts,contractService) {
    var cc = this;

    cc.toggleCustomerDetails = false;
    cc.disableEditCustomerDetails = true;
    cc.showSaveButton = false;
    cc.errorType = '';
    cc.genders = ['FEMALE','MALE'];

    cc.selectedCustomer = {};
    cc.countries = countries;

    //data table
    cc.customerItems = customerDetails.result.items;
    cc.sortKey = "";
    cc.reverse =false;

    //customer edit disableEditCustomerDetailscc.notifValue = '';
    cc.showError = false;
    cc.showSuccessNotif = false;
    cc.notifValue="";
    cc.inputTypeTextToNumber="text";
    cc.repeatSelectContracts = [];
    cc.contractList = contracts.result.items;
    cc.datePattern=/^(0?[1-9]|[12][0-9]|3[01])\-(0?[1-9]|1[012])\-([0-9]{4})$/i;


    //functions
    cc.viewCustomerNav = viewCustomerNav;
    cc.closeCustomerDetailsPanel = closeCustomerDetailsPanel;
    cc.enableCustomerEditMode = enableCustomerEditMode;
    cc.disableCustomerEditMode = disableCustomerEditMode;
    cc.saveUpdatedCustomer = saveUpdatedCustomer;
    cc.showCreateNewCustomerModal = showCreateNewCustomerModal;
    cc.removeCustomerContract = removeCustomerContract;
    cc.removeContractRowDropdown = removeContractRowDropdown;
    cc.addAnotherContractDropdown = addAnotherContractDropdown;
    cc.sort = sort;
    cc.clickExportButton = clickExportButton;

    function removeCustomerContract(index,data){
      // remove the row specified in index
      cc.customerItems.splice( index, 1);
      // if no rows left in the array create a blank array
      if (cc.customerItems.length === 0){
        cc.customerItems = [];
      }

      service.deleteCustomerContract(data.customerNumber).then(function (data) {
        if(!data.error){
          cc.selectedCustomer = {};
          cc.toggleCustomerDetails = false;
          $route.reload();
        }
        else{
          alert("error");
        }
      });
    }

    function viewCustomerNav(customerData){
      service.getExistingCustomerContracts(customerData.customerNumber).then(function (data) {
        var birthday = moment(data.result.customerDTO.birthday, [moment.ISO_8601]).format('DD-MM-YYYY');
        console.log(data.result);
        if(!data.error){
          cc.selectedCustomer = data.result;
          cc.selectedCustomer.customerDTO.birthdayStr = birthday;
          cc.repeatSelectContracts = cc.selectedCustomer.customerContractDTO.contractNum;
          cc.toggleCustomerDetails = true;
        }
        else{
          alert("error");
        }
      });

      cc.disableCustomerEditMode();
    }

    function closeCustomerDetailsPanel() {
      cc.toggleCustomerDetails = false;
      cc.disableEditCustomerDetails = true;
      cc.selectedCustomer = {};
      cc.showSaveButton = false;
    }

    function enableCustomerEditMode() {
      cc.disableEditCustomerDetails = false;
      cc.showSaveButton = true;
      cc.inputTypeTextToNumber="number";
    }

    function disableCustomerEditMode() {
      cc.disableEditCustomerDetails = true;
      cc.showSaveButton = false;
    }

    function saveUpdatedCustomer(customerDetailForm) {
      if(customerDetailForm.birthdayStr.$error.pattern){
        cc.notifValue = 'Date format is incorrect';
        cc.showError = true;
        cc.showSuccessNotif = false;
        return "";
      }
      else if(hasDuplicates(cc.selectedCustomer.customerContractDTO.contractNum)){
        cc.notifValue = 'Duplicate selected contract';
        cc.showError = true;
        cc.showSuccessNotif = false;
        return "";
      }
      service.updateCustomerContract(cc.selectedCustomer).then(function (data) {
        if(data == "Failed"){
          cc.notifValue = 'Unable to save Customer and Contract Record';
          cc.showError = true;
          cc.showSuccessNotif = false;
        }
        else if(data == "503"){
          cc.notifValue = 'Duplicate Email';
          cc.showError = true;
          cc.showSuccessNotif = false;
        }
        else{
          cc.notifValue = 'Customer Record is updated';
          cc.newCustomer = null;
          cc.showError = false;
          cc.showSuccessNotif = true;
          $route.reload();
        }

      });
    }

    function showCreateNewCustomerModal() {
      ModalService.showModal({
        templateUrl: 'modules/customer/customer-create-modal.html',
        controller: "CustomerCreateController",
        inputs : {
          countries : cc.countries,
          genders : cc.genders,
          contracts : contracts
        }
      }).then(function(modal) {
        modal.element.modal();
        modal.close.then(function(result) {
        });
      });
    }

    function removeContractRowDropdown(index) {
      cc.repeatSelectContracts.splice( index, 1);
      // if no rows left in the array create a blank array
      if (cc.repeatSelectContracts.length === 0){
        cc.repeatSelectContracts = [];
      }
    }

    function addAnotherContractDropdown() {
      if(cc.repeatSelectContracts == undefined) {
        cc.repeatSelectContracts = [""];
        cc.selectedCustomer.customerContractDTO.contractNum = [];
      }
      else
      {
        cc.repeatSelectContracts.push(1);
      }
    }

    function sort(keyname) {
      cc.sortKey = keyname;   //set the sortKey to the param passed
      cc.reverse = !cc.reverse; //if true make it false and vice versa
    }

    function hasDuplicates(a) {
      return _.uniq(a).length !== a.length;
    }

    function clickExportButton(contractNum) {

      //connect to contract Service to retrieve title and body
      contractService.getExistingContract(contractNum).then(function (data) {
        if(!data.error){
          var doc = new jsPDF();

          doc.setFontSize(25);
          doc.text(70,20, "Customer Agreement");
          doc.setFontSize(14);
          doc.text(20,30, "Contract ID: "+contractNum);
          doc.text(20,40, "Title: "+data.result.title);


          if(cc.selectedCustomer.customerDTO.gender == "FEMALE") {
            doc.text(20,60, "To Ms. "+ cc.selectedCustomer.customerDTO.firstName + " "
            + cc.selectedCustomer.customerDTO.lastName +",");

          }
          else
          {
            doc.text(20,60, "To Mr. "+ cc.selectedCustomer.customerDTO.firstName + " "
            + cc.selectedCustomer.customerDTO.lastName +",");
          }

          doc.setFontSize(12);
          doc.setLineWidth(50);

          var text_arr = data.result.body.match(/.{1,80}/g);

          for (i = 0; i < text_arr.length; i++) {
            doc.text(20,80+([i]*5), text_arr[i]);
          }

          doc.save(cc.selectedCustomer.customerDTO.lastName+"_"+cc.selectedCustomer.customerDTO.firstName+"_"+contractNum);
        }
        else{
          alert("error");
        }
      });

    }

  }

  return controller;
});
