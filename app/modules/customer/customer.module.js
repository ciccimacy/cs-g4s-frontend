define([
'angular',
'angular-route',
'./customer.config',
'./customer.controller',
'./customer.create.controller',
'./customer.service',
'customer-endpoint-service',
'contract/contract.module',
'dir-paginate',
'angularModalService',
'lodash',
'jspdf'
], function (angular,angularRoute, config, customercontroller,customercreatecontroller,service,customerEndpointService,angularModalService) {
  'use strict';

  angular.module('customerModule', ['ngRoute','angularUtils.directives.dirPagination','contractModule'])
		  .controller('CustomerController', customercontroller)
      .controller('CustomerCreateController', customercreatecontroller)
		  .service('customerService', service)
      .service('customerEndpointService', customerEndpointService)
		  .config(config);
});
