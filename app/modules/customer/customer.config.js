define(['require'], function (require) {
  'use strict';

  function config($routeProvider,$locationProvider) {
    $routeProvider.when('/customer', {
      templateUrl: require.toUrl('./customer.html'),
      controller: 'CustomerController',
      controllerAs: 'cc',
      resolve: {
        customerDetails: ['customerService', function (customerService) {
          var result = customerService.getCustomers();
          return result;
        }],
        countries: ['customerService', function (customerService) {
          var result = customerService.getCountries();

          return result;
        }],
        contracts: ['contractService', function (contractService) {
          var result = contractService.getContracts();

          return result;
        }]
      }
    });
        // use the HTML5 History API
    $locationProvider.html5Mode(true);
  }

  config.$inject = ['$routeProvider','$locationProvider'];

  return config;
});
