define([], function () {
  'use strict';

  function customerEndpointService ($q) {

    return {
      getCustomers : function(){
        var p = $q.defer();

        var customerDetails = [{
          firstName: 'Cicciolina',
          lastName: 'Miranda',
          email: 'cicci.miranda@gmail.com'
        },
        {
          firstName: 'Macy',
          lastName: 'Miranda',
          email: 'macy@miranda@gmail.com'
        },
        {
          firstName: 'Test',
          lastName: 'Miranda',
          email: 'test@gmail.com'
        },
        {
          firstName: 'Cicciolina',
          lastName: 'Miranda',
          email: 'cicci.miranda@gmail.com'
        },
        {
          firstName: 'Macy',
          lastName: 'Miranda',
          email: 'macy@miranda@gmail.com'
        },
        {
          firstName: 'Test',
          lastName: 'Miranda',
          email: 'test@gmail.com'
        },
        {
          firstName: 'Cicciolina',
          lastName: 'Miranda',
          email: 'cicci.miranda@gmail.com'
        },
        {
          firstName: 'Macy',
          lastName: 'Miranda',
          email: 'macy@miranda@gmail.com'
        },
        {
          firstName: 'Test',
          lastName: 'Miranda',
          email: 'test@gmail.com'
        },
        {
          firstName: 'Cicciolina',
          lastName: 'Miranda',
          email: 'cicci.miranda@gmail.com'
        },
        {
          firstName: 'Macy',
          lastName: 'Miranda',
          email: 'macy@miranda@gmail.com'
        },
        {
          firstName: 'Test',
          lastName: 'Miranda',
          email: 'test@gmail.com'
        }
          ];


        p.resolve(customerDetails);

        return p.promise;
      },

      getExistingCustomerContracts : function(customerNumber){
        var p = $q.defer();
        var customerContract = {
         "customerDTO": {
          "firstName": "Cicci",
          "lastName": "Awesome",
          "gender": "FEMALE",
          "phoneNumber": "9171212123",
          "birthday": "1867-05-05T00:00:00.000+08:00",
          "companyName": "test company",
          "email": "m@m.com",
          "streetAddress": "test",
          "city": "test city",
          "postcode": 1111,
          "country": "BH",
          "customerNumber": "REF33418280"
         },
         "customerContractDTO": {
          "customerNumber": "REF33418280",
          "contractNum": [
           "CONTRACT80841550",
           "CONTRACT13225450",
           "CONTRACT28454981",
           "CONTRACT82290345"
          ]
         }
       };

        p.resolve(customerContract);
        return p.promise;
      },

      addCustomerContact : function(customerContract){
        var p = $q.defer();
        var key = 1234567;
        p.resolve(key);
        return p.promise;
      },

      updateCustomerContract : function(customerContract){
        var p = $q.defer();
        var key = 77777;
        p.resolve(key);
        return p.promise;
      }
    };
  }

  customerEndpointService.$inject = ['$q'];

  return customerEndpointService;
});
