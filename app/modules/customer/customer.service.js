define([], function () {
  'use strict';
  var customerService = function service($http, /**authenticationService, **/customerEndpointService) {
    return {
      getCustomers: function () {

        return customerEndpointService.getCustomers().then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      },

      getExistingCustomerContracts: function (customerNumber) {

        return customerEndpointService.getExistingCustomerContracts(customerNumber).then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      },

      addCustomerContact: function (customerContract) {
        return customerEndpointService.addCustomerContact(customerContract).then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      },

      updateCustomerContract: function (customercontract) {
        return customerEndpointService.updateCustomerContract(customercontract).then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      },

      deleteCustomerContract: function (customerNumber) {
        return customerEndpointService.deleteCustomerContract(customerNumber).then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      },

      getCountries: function () {

        return customerEndpointService.getCountries().then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      }

    };
  };

  customerService.$inject = ['$http', /**'authenticationService',**/ 'customerEndpointService'];

  return customerService;
});
