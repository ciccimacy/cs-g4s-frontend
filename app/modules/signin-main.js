require.config({
  paths: {
    //cryptojs: '//crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha256',
    angular: '../../bower_components/angular/angular',
    bootstrap: '../../bower_components/bootstrap/dist/js/bootstrap',
    async: '../../bower_components/requirejs-plugins/src/async',
    jquery: '../../bower_components/jquery/dist/jquery',
    'angular-route': '../../bower_components/angular-route/angular-route',
     'angular-cookies': '../../bower_components/angular-cookies/angular-cookies',
     'angular-google-gapi': '../../bower_components/angular-google-gapi/angular-google-gapi'
  },
  shim: {
    bootstrap: {
      deps: [
        'jquery'
      ]
    },
    angular: {
      exports: 'angular'
    },
    'angular-route': [
      'angular'
    ],
    'angular-cookies': [
      'angular'
    ],
    cryptojs: {
      exports: 'CryptoJS'
    }
  },
  packages: []
});
require([
  'bootstrap',
  'angular',
  'login/login.module',
  'async!https://apis.google.com/js/client.js!onload'
], function () {
  'use strict';

  bootstrap();
  //Load Google APIs here
  function loadGoogleApis() {
    var apiList = [
      {name: 'secured', version: 'v1', url: 'https://cs-g4s-javelin-secure.appspot.com/_ah/api'}
    ];

    var apisToLoad = apiList.length;
    for (var i = 0; i < apiList.length; i++) {
      var api = apiList[i];
    //  NProgress.inc();
      console.log('Loading ' + api.name + '...');
      gapi.client.load(api.name, api.version, loadedApiCallback, api.url);
    }

    function loadedApiCallback() {
      if (--apisToLoad === 0) {
      //  NProgress.done();
        console.log("Endpoints Service is ready!");
      }
    }
  }

  function bootstrap() {
    console.log('Bootstrapping modules...');
    //NProgress.inc();
    angular.bootstrap(document, ['loginModule']);
    loadGoogleApis();
    console.log('Modules bootstrapped!');
  }

});
