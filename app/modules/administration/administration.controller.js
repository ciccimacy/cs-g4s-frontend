define([], function () {
  'use strict';

  controller.$inject = ['$route','$cookies','userAccounts','administrationService','ModalService'];

  function controller($route,$cookies,userAccounts,service,ModalService) {
    var ad = this

    //manage profile properties
    ad.isEditAccountDisabled = true;
    ad.isManageProfileOpen = false;

    //user account list properties
    ad.isUserAccountListOpen = false;
    ad.userAccountList = userAccounts.result.items;
    ad.sortKey = "";
    ad.reverse =false;

    //new user account
    ad.newUserAccount = {
      email:'',
      firstName:'',
      lastName:'',
      admin:false
    };

    //loggedUserEmail
    ad.loggedUserEmail = $cookies.getObject('sessionId');
    ad.userprofile = {};

    //password inputtype
    ad.passwordInputType = 'password';
    //--functions--//

    //manage profile tab
    ad.enableProfileEditMode = enableProfileEditMode;
    ad.disabledProfileEditMode = disabledProfileEditMode;
    ad.getLoggedUser = getLoggedUser;
    ad.hideShowPassword = hideShowPassword;
    ad.saveUpdatedLoggedUser = saveUpdatedLoggedUser;

    //user accounts list
    ad.openUserAccountList = openUserAccountList;
    ad.openAddNewUser = openAddNewUser;
    ad.saveNewUserAccount = saveNewUserAccount;
    ad.removeUserAccount = removeUserAccount;
    ad.sort = sort;

    getLoggedUser();

    function enableProfileEditMode() {
      ad.isEditAccountDisabled = false;
    }

    function disabledProfileEditMode() {
      $route.reload();
    }

    function openUserAccountList() {
      ad.isManageProfileOpen = false;
      if(!ad.isUserAccountListOpen){
        ad.isUserAccountListOpen = true;
      }
      else{
        ad.isUserAccountListOpen = false;
      }
    }
    function saveNewUserAccount() {
      return service.addUserAccount(ad.newUserAccount).then(function (data) {
        if(!data.error){
          alert("save in the database");
        }else{
          alert("error");
        }
        ad.newUserAccount = null;
        $route.reload();
        return data;
      });
    }

    function removeUserAccount(index,data){
      // remove the row specified in index
      ad.userAccountList.splice( index, 1);
      // if no rows left in the array create a blank array
      if (ad.userAccountList.length === 0){
        ad.userAccountList = [];
      }

      service.deleteUserAccount(data.email).then(function (data) {
        if(data.error){
          alert("error");
        }
        $route.reload();
      });
    }

    function openAddNewUser() {
      ModalService.showModal({
        templateUrl: 'modules/administration/administration-createuser-modal.html',
        controller: "NewUserAccountController"
      }).then(function(modal) {
        modal.element.modal();
        modal.close.then(function(result) {
        });
      });
    }

    function getLoggedUser() {
      return service.getLoggedUser(ad.loggedUserEmail).then(function (data) {
        if(data.error){
          alert("error");
          return "";
        }
        ad.userprofile = data.result;
        return data;
      });
    }

    function hideShowPassword() {
      if (ad.passwordInputType == 'password')
      ad.passwordInputType = 'text';
      else
      ad.passwordInputType = 'password';
    }

    function saveUpdatedLoggedUser() {
      return service.updateLoggedUser(ad.userprofile).then(function (data) {
        if(data.error){
          alert("error");
          return "";
        }
        $route.reload();
        return data;
      });
    }

    function sort(keyname) {
      ad.sortKey = keyname;   //set the sortKey to the param passed
      ad.reverse = !ad.reverse; //if true make it false and vice versa
    }
  }

  return controller;
});
