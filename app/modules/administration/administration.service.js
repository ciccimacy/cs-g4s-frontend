define([], function () {
  'use strict';
  var administrationService = function service($http, /**authenticationService, **/administrationEndpointService) {
    return {
      getAllUserAccount: function () {

        return administrationEndpointService.getAllUserAccount().then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      },

      getLoggedUser: function (email) {

        return administrationEndpointService.getLoggedUser(email).then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      },

      addUserAccount: function (userAccount) {
        return administrationEndpointService.addUserAccount(userAccount).then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      },

      updateLoggedUser: function (userAccount) {
        return administrationEndpointService.updateLoggedUser(userAccount).then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      },

        deleteUserAccount: function (email) {
        return administrationEndpointService.deleteUserAccount(email).then(function (response) {
          return response;
        }, function(error) {
          return error;
        });
      }
    };
  };

  administrationService.$inject = ['$http', 'administrationEndpointService'];

  return administrationService;
});
