define([], function () {
  'use strict';

  function administrationEndpointService ($q) {

    return {
      getAllUserAccount : function(){
        var p = $q.defer();
        gapi.client.secured.useraccount.list()
        .then(function(response){
          console.log("Success: " + response.result.items);
          p.resolve(response);
        },function(response){
          console.log("Fail: " + response.result);
          p.reject(response);
        });
        return p.promise;
      },

      getLoggedUser : function(email){
        var p = $q.defer();
        gapi.client.secured.useraccount.get({'email' : email})
        .then(function(response){
          console.log("Success: " + response.result);
          p.resolve(response);
        },function(response){
          console.log("Fail: " + response.result);
          p.reject(response);
        });
        return p.promise;
      },

      updateLoggedUser : function(useraccount){
        var p = $q.defer();
        gapi.client.secured.useraccount.update({resource:useraccount}).execute(function(response) {
          console.log("Success: " + response.result);
          if(response.status !== 200){
            p.resolve("");
          }
          p.resolve(response);
        },function(response){
          console.log("Fail: " + response);
          p.reject("Service Unavailable");
        });
        return p.promise;
      },

      addUserAccount : function(userAccount){
        var p = $q.defer();
        gapi.client.secured.useraccount.create({resource:userAccount}).execute(function(response) {
          console.log("Success: " + response.result);
          if(response.error != undefined){
            p.resolve("Duplicate Email");
          }
          else{
            p.resolve(response);
          }

        },function(response){
          console.log("Fail: " + response);
          p.reject("Service Unavailable");
        });
        return p.promise;
      },

      deleteUserAccount : function(email){
        var p = $q.defer();
        gapi.client.secured.useraccount.delete({'email' : email}).execute(function(response) {
          console.log("Success: " + response.result);
          if(response.status !== 200){
            p.resolve("");
          }
          p.resolve(response);
        },function(response){
          console.log("Fail: " + response);
          p.reject("Service Unavailable");
        });
        return p.promise;
      }

    };
  }

  administrationEndpointService.$inject = ['$q'];

  return administrationEndpointService;
});
