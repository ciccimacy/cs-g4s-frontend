define([
  'angular',
  'angular-route',
  './administration.config',
  './administration.controller',
  './administration.createuser.controller',
  './administration.service',
  'administration-endpoint-service',
  'dir-paginate',
  'angularModalService',
  'angular-cookies'
], function (angular,angularRoute, config,controller,createusercontroller,service,administrationEndpointService,dirPaginate,angularModalService,angularCookies) {
  'use strict';

  angular.module('administrationModule', ['ngRoute','angularUtils.directives.dirPagination','angularModalService','ngCookies'])
  .controller('AdministrationController', controller)
  .controller('NewUserAccountController', createusercontroller)
  .service('administrationService', service)
  .service('administrationEndpointService', administrationEndpointService)
  .config(config);
});
