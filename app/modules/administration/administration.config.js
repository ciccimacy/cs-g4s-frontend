define(['require'], function (require) {
  'use strict';

  config.$inject = ['$routeProvider'];

  function config($routeProvider) {
    $routeProvider.when('/userprofile', {
      templateUrl: require.toUrl('./administration-userprofile.html'),
      controller: 'AdministrationController',
      controllerAs: 'ad',
      resolve: {
        userAccounts: ['administrationService', function (administrationService) {
          var result = administrationService.getAllUserAccount();
          console.log(result);
          return result;
        }]
      }
    }).when('/useracounts', {
      templateUrl: require.toUrl('./administration-userlist.html'),
      controller: 'AdministrationController',
      controllerAs: 'ad',
      resolve: {
        userAccounts: ['administrationService', function (administrationService) {
          var result = administrationService.getAllUserAccount();
          console.log(result);
          return result;
        }]
      }
    });
  }

  return config;
});
