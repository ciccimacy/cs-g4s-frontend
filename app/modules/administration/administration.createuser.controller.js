define([], function () {
  'use strict';

  controller.$inject = ['$scope','$route','administrationService'];

  function controller($scope,$route,service) {

    $scope.adminRightsOption = [{label: 'Please select', value: null},{label: 'Yes', value: true},{label: 'No', value: false}];
    $scope.newUserAccount = {
      email:'',
      firstName:'',
      lastName:'',
      admin:null
    };

    $scope.notifValue = '';
    $scope.showError = false;
    $scope.showSuccessNotif = false;
    //--functions--//S
    $scope.saveNewUserAccount = saveNewUserAccount;

    function saveNewUserAccount() {

        console.log($scope.newUserAccount);
        service.addUserAccount($scope.newUserAccount).then(function (data) {
          if(data == "Duplicate Email"){
            $scope.notifValue = 'Email is already existing';
            $scope.showError = true;
            $scope.showSuccessNotif = false;
          }else{
            $scope.notifValue = 'User account is saved';
            $scope.newUserAccount = null;
            $scope.showError = false;
            $scope.showSuccessNotif = true;
            $route.reload();
          }
        });

    }



  }

  return controller;
});
