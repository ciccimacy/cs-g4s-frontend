define([], function () {
  'use strict';

  function administrationEndpointService ($q) {

    return {
      getAllUserAccount : function(){
        var p = $q.defer();
        var userAccountList = [
          {
            "email": "admin@admin.com",
            "firstName": "admin",
            "lastName": "admin",
            "admin": true,
            "password": "admin"
          },
          {
            "email": "cicci.miranda@gmail.com",
            "firstName": "Cicci",
            "lastName": "Miranda",
            "admin": true,
            "password": "MACYcute143"
          },
          {
            "email": "useracount@user.com",
            "firstName": "User",
            "lastName": "Account",
            "admin": true,
            "password": "newpassword"
          },
          {
            "email": "test@admin.com",
            "firstName": "testadmin",
            "lastName": "nonadmin",
            "admin": false,
            "password": "nonadmin"
          }

        ];
        p.response(userAccountList);
        return p.promise;
      },

      getLoggedUser : function(email){
        var p = $q.defer();
        var loggedUser = {
          email: "useracount@user.com", firstName: "User", lastName: "Account", admin: "true"
        };
        p.response(loggedUser);
        return p.promise;
      },

      updateLoggedUser : function(useraccount){
        var p = $q.defer();
        var key = 123345;
        p.response(key);
        return p.promise;
      },

      addUserAccount : function(userAccount){
        var p = $q.defer();
        var key = 11111;
        p.response(key);
        return p.promise;
      }

    };
  }

  administrationEndpointService.$inject = ['$q'];

  return administrationEndpointService;
});
