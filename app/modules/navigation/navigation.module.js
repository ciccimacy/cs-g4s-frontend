define([
  'navigation/navigation.config',
  'navigation/navigation.controller',
  'angular',
  'angular-route',
  'authentication/authentication.module',
  'angular-cookies'
], function (config, controller,angularCookies) {
  'use strict';

  var app = angular.module('navigationModule', ['ngRoute','authenticationModule','ngCookies']);
  app.config(config);
  app.controller('NavCtrl', controller);

});
