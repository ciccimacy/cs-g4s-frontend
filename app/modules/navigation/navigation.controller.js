define([], function () {
  'use strict';

  controller.$inject = ['authenticationService', '$window', '$location','$cookies'];

  function controller(authenticationService, $window, $location,$cookies) {
    var _this = this;
    _this.activeTab = $location.path();
    _this.logout = logout;
    _this.changeTab = changeTab;
    _this.loggedUser = {
      email :  $cookies.getObject('sessionId'),
      admin : $cookies.getObject('admin')
    };

    activate();

    function activate() {
      if (_this.activeTab === '/' || _this.activeTab === '') {
        _this.activeTab = '/customer';
      }
    }

    function changeTab(tabName) {
      _this.activeTab = tabName;
    }

    function logout() {
      authenticationService.logout();
      $window.location.href = '/';
    }
  }

  return controller;
});
