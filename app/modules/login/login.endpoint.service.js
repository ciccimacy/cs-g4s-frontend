define([], function () {
  'use strict';

  function loginService ($q) {


    return {

      login :function(email, pass) {
        //return $q.when({data: {id: 1231}});
        var p = $q.defer();
        gapi.client.secured.useraccount.verify({'email' : email, 'password' : pass })
        .then(function(response){
          if(response.error != undefined){
            p.resolve("Failed");
          }
          else{
            p.resolve(response);
          }
        },function(response){
          p.resolve("Failed");
        });
        return p.promise;
      }


    };
  }

  loginService.$inject = ['$q'];

  return loginService;
});
