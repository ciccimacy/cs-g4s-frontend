define([
  'authentication/authentication.service',
  'angular',
  'angular-route',
  'angular-cookies'
],function(service){
  'use strict';

  var auth = angular.module('authenticationModule',['ngRoute', 'ngCookies']);
  auth.service('authenticationService', service);

});